const form = document.querySelector('form');
const passwordInput = document.getElementById('inputPassword');
const confirmPasswordInput = document.getElementById('inputConfirmPassword');
const passwordMatchMessage = document.getElementById('passwordMatch');

passwordInput.addEventListener('input', validatePassword);
confirmPasswordInput.addEventListener('input', validatePassword);

function validatePassword() {
    const password = passwordInput.value.trim();
    const confirmPassword = confirmPasswordInput.value.trim();

    // Если пароли не совпадают или одно из полей пустое, показываем сообщение об ошибке
    if (password !== confirmPassword || password === '' || confirmPassword === '') {
        passwordMatchMessage.textContent = 'Пароли не совпадают';
        passwordMatchMessage.style.color = 'red';
    } else {
        // Иначе показываем сообщение о совпадении паролей
        passwordMatchMessage.textContent = '';
        passwordMatchMessage.style.color = 'green';
    }
}

function isValidEmail(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

// Функция для проверки валидности полей, содержащих только кириллические символы
function isValidCyrillicName(name) {
    return /^[А-ЯЁ][а-яё]+$/.test(name);
}

form.addEventListener('submit', function (event) {
    event.preventDefault(); // Предотвращаем стандартное поведение отправки формы

    const email = document.getElementById('exampleInputEmail').value.trim();
    const surname = document.getElementById('exampleInputSurname').value.trim();
    const name = document.getElementById('exampleInputName').value.trim();
    const patronymic = document.getElementById('exampleInputPatronymic').value.trim();

    if (!isValidEmail(email)) {
        alert('Введите корректный email');
        return;
    }
    if (passwordInput.value.length < 5) {
        alert('Пароль должен быть не менее 5 символов в длину');
        return;
    }

    // Вызываем функцию для проверки совпадения паролей перед отправкой формы
    validatePassword();

    // Если пароли не совпадают, прерываем отправку формы
    if (passwordMatchMessage.style.color === 'red') {
        alert('Пароли не совпадают');
        return;
    }

    if (!isValidCyrillicName(surname)) {
        alert('Введите корректную фамилию');
        return;
    }
    if (!isValidCyrillicName(name)) {
        alert('Введите корректное имя');
        return;
    }
    if (!isValidCyrillicName(patronymic)) {
        alert('Введите корректное отчество');
        return;
    }
    form.submit();
});