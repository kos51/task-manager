const form = document.querySelector('form');
form.addEventListener('submit', function (event) {
    event.preventDefault(); // Предотвращаем стандартное поведение отправки формы

    const email = document.getElementById('exampleInputEmail').value;
    const password = document.getElementById('exampleInputPassword').value;

    // Проверка валидности email
    if (!isValidEmail(email)) {
        alert('Введите корректный email');
        return;
    }

    // Проверка валидности пароля
    if (password.length < 5) {
        alert('Пароль должен быть не менее 5 символов в длину');
        return;
    }

    // Если все поля валидны, отправляем форму
    form.submit();
});

// Функция для проверки валидности email
function isValidEmail(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

const eyeButton = document.querySelector('.eye-button');
const passwordField = document.querySelector('#exampleInputPassword');

eyeButton.addEventListener('click', function() {
    const fieldType = passwordField.getAttribute('type');
    if (fieldType === 'password') {
        passwordField.setAttribute('type', 'text');
    } else {
        passwordField.setAttribute('type', 'password');
    }
});