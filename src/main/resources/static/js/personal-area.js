function openCreateTaskModal() {
    const createTaskModal = document.getElementById('createTaskModal');
    createTaskModal.style.display = 'block';
}

function closeModal(modalId) {
    const modal = document.getElementById(modalId);
    modal.style.display = "none";
}

// Закрытие модального окна при щелчке за его пределами или по кнопке "X"
window.onclick = function (event) {
    const createTaskModal = document.getElementById("createTaskModal");
    const updateTaskModal = document.getElementById("updateTaskModal");
    const weeklyDeadlineModal = document.getElementById('weeklyDeadlineModal');
    const closeBtn = document.getElementsByClassName("close")[0];

    if (event.target === createTaskModal || event.target === closeBtn) {
        closeModal("createTaskModal");
    }
    if (event.target === updateTaskModal || event.target === closeBtn) {
        closeModal("updateTaskModal");
    }
    if (event.target === weeklyDeadlineModal || event.target === closeBtn) {
        closeModal("weeklyDeadlineModal");
    }
}

// Закрытие модального окна при нажатии клавиши Esc
document.addEventListener('keydown', function (event) {
    const createTaskModal = document.getElementById("createTaskModal");
    const updateTaskModal = document.getElementById("updateTaskModal");

    if (event.key === "Escape" && createTaskModal.style.display === 'block') {
        closeModal("createTaskModal");
    } else if (event.key === "Escape" && updateTaskModal.style.display === 'block') {
        closeModal("updateTaskModal");
    }
});

//Функция для переноса строки в модальном окне в поле ввода резолюции и наименования при нажатии ctrl+enter, работает и с Enter
document.addEventListener('DOMContentLoaded', function () {
    const commentInput = document.getElementById('commentInput');
    const taskNameInput = document.getElementById('taskName');

    const handleEnterWithCtrl = function (event) {
        if (event.key === 'Enter' && event.ctrlKey) {
            // Вставляем новую строку с установкой курсора
            const cursorPosition = this.selectionStart;
            const textBeforeCursor = this.value.substring(0, cursorPosition);
            const textAfterCursor = this.value.substring(cursorPosition);
            this.value = textBeforeCursor + '\n' + textAfterCursor;

            // Устанавливаем курсор на новую строку
            this.selectionStart = cursorPosition + 1;  // 1 - длина символа новой строки
            event.preventDefault();
        }
    };
    commentInput.addEventListener('keydown', handleEnterWithCtrl);
    taskNameInput.addEventListener('keydown', handleEnterWithCtrl);
});

// Функция для установки значения по умолчанию
function setDefaultValue(inputElement) {
    const currentDatetime = new Date();
    const year = currentDatetime.getFullYear();
    const month = ('0' + (currentDatetime.getMonth() + 1)).slice(-2); // Месяцы начинаются с 0
    const day = ('0' + currentDatetime.getDate()).slice(-2);
    const hours = ('0' + currentDatetime.getHours()).slice(-2);
    const minutes = ('0' + currentDatetime.getMinutes()).slice(-2);

    if (inputElement.type === 'datetime-local') {
        // Если тип инпута datetime-local, устанавливаем дату и время
        inputElement.value = `${year}-${month}-${day}T${hours}:${minutes}`;
    } else if (inputElement.type === 'time') {
        // Если тип инпута time, устанавливаем только время
        inputElement.value = `${hours}:${minutes}`;
    }
}

// Функция для установки значения по умолчанию для select дня недели
function setDefaultDayOfWeek(selectElement) {
    selectElement.selectedIndex = (new Date().getDay() + 6) % 7;
}

// Вызываем установку значения по умолчанию для select дня недели при загрузке страницы
document.addEventListener('DOMContentLoaded', function () {
    const weeklyDaySelect = document.getElementById('weeklyDay');
    const updateWeeklyDaySelect = document.getElementById('updateWeeklyDay');
    const weeklyTimeSelect = document.getElementById('weeklyTime');
    const updateWeeklyTimeSelect = document.getElementById('updateWeeklyTime');

    if (weeklyDaySelect) {
        setDefaultDayOfWeek(weeklyDaySelect);
    }
    if (updateWeeklyDaySelect) {
        setDefaultDayOfWeek(updateWeeklyDaySelect);
    }
    if (weeklyTimeSelect) {
        setDefaultValue(weeklyTimeSelect);
    }
    if (updateWeeklyTimeSelect) {
        setDefaultValue(updateWeeklyTimeSelect);
    }
});

// Слушатель события для кнопки "X" вместо использования атрибута onclick (для удаления задачи не в модальном окне)
document.addEventListener('DOMContentLoaded', function () {
    const taskRows = document.querySelectorAll('.task-row');

    taskRows.forEach(function (row) {
        row.addEventListener('click', function (event) {
            // Предотвращаем открытие модального окна при клике на кнопку "X"
            if (event.target.closest('.delete-task-button')) {
                event.preventDefault(); // Отменяем стандартное действие по умолчанию
                event.stopPropagation(); // Предотвращаем всплытие события
                if (confirm("Вы уверены, что хотите удалить задачу?")) {
                    // Если пользователь подтвердил удаление, отправляем форму
                    event.target.closest('form').submit();
                }
            } else {
                openUpdateTaskModal(row);
            }
        });
    });
});

$(document).ready(function () {
    // Функция для сортировки таблицы
    function sortTable(columnIndex, currentStatus) {
        const table = $(".table");
        const rows = $("tbody tr", table).toArray();

        rows.sort(function (a, b) {
            const aValue = $("td", a).eq(columnIndex).text();
            const bValue = $("td", b).eq(columnIndex).text();

            // Сравнение значений с учетом текущего статуса
            if (currentStatus === "на исполнении") {
                return aValue.localeCompare(bValue);
            } else {
                return bValue.localeCompare(aValue);
            }
        });

        $("tbody", table).empty().append(rows);
    }

    // Обработчик клика по кнопке сортировки статуса
    $(".sort-status-button").click(function () {
        const columnIndex = $(this).data("column-index");
        const currentStatus = $(this).data("status");
        sortTable(columnIndex, currentStatus);

        // Переключение текущего статуса для следующей сортировки
        if (currentStatus === "на исполнении") {
            $(this).data("status", "завершено");
        } else {
            $(this).data("status", "на исполнении");
        }
    });
});

let showCommentTimeout;

// Функция для показа комментария "изменить" с задержкой
function showEditComment(event) {
    clearTimeout(showCommentTimeout); // Сбрасываем предыдущий таймер
    showCommentTimeout = setTimeout(function () {
        const editComment = document.getElementById('edit-comment');
        if (editComment && !event.target.classList.contains('delete-task-button')) {
            const xOffset = 10; // Смещение по горизонтали
            const yOffset = 10; // Смещение по вертикали
            const x = event.clientX + xOffset;
            const y = event.clientY + yOffset;
            editComment.style.left = x + 'px';
            editComment.style.top = y + 'px';
            editComment.classList.add('active');
        }
    }, 800); // Задержка в миллисекундах (в данном случае 1000 мс или 1 секунда)
}

// Функция для скрытия комментария "изменить"
function hideEditComment() {
    clearTimeout(showCommentTimeout); // Сбрасываем таймер перед показом комментария
    const editComment = document.getElementById('edit-comment');
    if (editComment) {
        editComment.classList.remove('active');
    }
}

document.addEventListener('DOMContentLoaded', function () {
    // Получаем все строки таблицы с классом 'task-row'
    const taskRows = document.querySelectorAll('.task-row');
    // Привязываем к каждой строке функции открытия и закрытия комментария
    taskRows.forEach(function (row) {
        row.addEventListener('mouseover', function (event) {
            showEditComment(event);
        });
        row.addEventListener('mouseout', function () {
            hideEditComment();
        });
    });
});

// Вызываем установку значения по умолчанию при загрузке страницы для создания задачи
document.addEventListener('DOMContentLoaded', function () {
    const createSpecificDeadlineInput = document.getElementById('createSpecificDeadline');
    const updateSpecificDeadlineInput = document.getElementById('updateSpecificDeadline');

    if (createSpecificDeadlineInput) {
        setDefaultValue(createSpecificDeadlineInput);
    }
    if (updateSpecificDeadlineInput) {
        setDefaultValue(updateSpecificDeadlineInput);
    }
});

//Функция для работы кнопки "добавить срок", также для кнопки "X"
function addDeadlineInput(containerId, deadlineType) {
    const deadlinesContainer = document.getElementById(containerId);
    if (!deadlinesContainer) {
        console.error("Container not found.");
        return;
    }
    const newInput = document.createElement('div');
    newInput.classList.add('deadline-input-container');

    const deadlineInput = document.createElement('input');
    deadlineInput.type = 'datetime-local';
    deadlineInput.name = (deadlineType === 'update') ? 'updateDeadlines' : 'deadlines';
    deadlineInput.required = true;
    setDefaultValue(deadlineInput);
    const deleteButton = document.createElement('button');
    deleteButton.innerHTML = '❌'; // Unicode for the delete icon
    deleteButton.classList.add('delete-deadline-button');
    deleteButton.addEventListener('click', function () {
        deadlinesContainer.removeChild(newInput);
    });
    newInput.appendChild(deadlineInput);
    newInput.appendChild(deleteButton);
    // Вставляем новый срок перед кнопкой "добавить срок"
    deadlinesContainer.insertBefore(newInput, deadlinesContainer.lastElementChild);
}

// Функция для открытия модального окна изменения задачи
function openUpdateTaskModal(taskRow) {
    document.getElementById('updateDeadlineType').addEventListener('change', handleDeadlineTypeChangeForUpdate);
    const updateTaskModal = document.getElementById('updateTaskModal');
    updateTaskModal.style.display = 'block';

    const taskId = taskRow.dataset.taskId;
    const taskName = taskRow.dataset.taskName;
    const taskComment = taskRow.dataset.taskComment;
    const taskStatus = taskRow.dataset.taskStatus;
    const taskPerformers = taskRow.dataset.taskPerformers ? taskRow.dataset.taskPerformers.split(',') : [];
    const taskDeadlines = taskRow.dataset.taskDeadlines ? taskRow.dataset.taskDeadlines.split(',') : [];
    const taskNotificationType = taskRow.dataset.taskNotificationType;
    const taskDayTimes = taskRow.dataset.taskDayTimes;

    document.querySelector('#updateTaskModal [name="taskId"]').value = taskId;
    document.querySelector('#updateTaskModal [name="taskName"]').value = taskName;
    document.querySelector('#updateTaskModal [name="comment"]').value = taskComment;
    document.querySelector('#updateTaskModal [name="status"]').value = taskStatus;

    const updatePerformersSelect = document.querySelector('#updateTaskModal [name="updatePerformers"]');
    for (let i = 0; i < updatePerformersSelect.options.length; i++) {
        if (taskPerformers.includes(updatePerformersSelect.options[i].value)) {
            updatePerformersSelect.options[i].selected = true;
        }
    }
    updateSelectedPerformers();

    // Выбираем тип сроков в зависимости от задачи
    const updateDeadlineTypeSelect = document.getElementById('updateDeadlineType');
    const updateDeadlinesContainer = document.getElementById('updateDeadlinesContainer');
    const updateWeeklyDeadlineContainer = document.getElementById('updateWeeklyDeadlineContainer');

    if (taskDeadlines.length > 0) {
        // Если есть сроки, выбираем тип "Конкретные сроки"
        updateDeadlineTypeSelect.value = 'DATES';
        updateDeadlinesContainer.style.display = 'block';
        updateWeeklyDeadlineContainer.style.display = 'none';

        // Устанавливаем значения сроков в инпуты
        updateDeadlinesContainer.innerHTML = '';
        for (let i = 0; i < taskDeadlines.length; i++) {
            const deadlineDate = parseDeadlineString(taskDeadlines[i]);
            deadlineDate.setHours(deadlineDate.getHours() + 3);

            const newInput = document.createElement('div');
            newInput.classList.add('deadline-input-container');

            const deadlineInput = document.createElement('input');
            deadlineInput.type = 'datetime-local';
            deadlineInput.name = 'updateDeadlines';
            deadlineInput.value = deadlineDate.toISOString().slice(0, 16);

            newInput.appendChild(deadlineInput);

            const deleteButton = document.createElement('button');
            deleteButton.innerHTML = '❌';
            deleteButton.classList.add('delete-deadline-button');
            deleteButton.addEventListener('click', function () {
                updateDeadlinesContainer.removeChild(newInput);
            });

            newInput.appendChild(deleteButton);

            updateDeadlinesContainer.appendChild(newInput);
        }
        const addDeadlineButton = document.createElement('button');
        addDeadlineButton.innerHTML = 'добавить срок';
        addDeadlineButton.id = 'addUpdateDeadlineButton';
        addDeadlineButton.type = 'button'; // Устанавливаем тип "button", чтобы предотвратить автоматическую отправку формы
        addDeadlineButton.addEventListener('click', function (event) {
            event.preventDefault(); // Предотвращаем отправку формы
            addDeadlineInput('updateDeadlinesContainer', 'update');
        });

        updateDeadlinesContainer.appendChild(addDeadlineButton);
    } else {
        // Если нет сроков, выбираем тип "Еженедельно"
        updateDeadlineTypeSelect.value = 'WEEKDAYS';
        updateDeadlinesContainer.style.display = 'none';
        updateWeeklyDeadlineContainer.style.display = 'block';

        fillWeeklyDeadlines(taskDayTimes);
    }

    // Устанавливаем значение типа напоминания
    const updateNotificationTypeSelect = document.getElementById('updateNotificationType');
    for (let i = 0; i < updateNotificationTypeSelect.options.length; i++) {
        const option = updateNotificationTypeSelect.options[i];
        if (option.value === taskNotificationType) {
            option.selected = true;
            break;
        }
    }
}

// Отображение для соответствия значений
const dayOfWeekMap = {
    "по понедельникам": "Понедельник",
    "по вторникам": "Вторник",
    "по средам": "Среда",
    "по четвергам": "Четверг",
    "по пятницам": "Пятница",
    "по субботам": "Суббота",
    "по воскресеньям": "Воскресенье"
};

// Функция для заполнения инпутов для сроков еженедельно
function fillWeeklyDeadlines(taskDayTimes) {
    const updateWeeklyDaySelect = document.getElementById('updateWeeklyDay');
    const updateWeeklyTimeInput = document.getElementById('updateWeeklyTime');

    // Очищаем значения полей перед заполнением
    updateWeeklyDaySelect.value = '';
    updateWeeklyTimeInput.value = '';

    // Если есть сроки, выбираем тип "Еженедельно"
    const updateDeadlineTypeSelect = document.getElementById('updateDeadlineType');
    updateDeadlineTypeSelect.value = 'WEEKDAYS';

    // Проходим по всем срокам и заполняем поля ввода
    const taskDayTimesArray = taskDayTimes.split(',');
    for (let i = 0; i < taskDayTimesArray.length; i++) {
        const dayTimeParts = taskDayTimesArray[i].split(" до ");

        // Находим соответствующий элемент в select по значению
        const selectedDay = dayOfWeekMap[dayTimeParts[0].trim()];
        const options = updateWeeklyDaySelect.options;

        for (let j = 0; j < options.length; j++) {
            if (options[j].text === selectedDay) {
                options[j].selected = true;
                break;
            }
        }

        // Заполняем инпут для времени
        updateWeeklyTimeInput.value = dayTimeParts[1].trim();
    }
}

function parseDeadlineString(deadlineString) {
    const parts = deadlineString.split(' до ');
    const dateString = parts[0];
    const timeString = parts[1];
    const [day, month, year] = dateString.split('.');
    const [hours, minutes] = timeString.split(':');

    return new Date(year, month - 1, day, hours, minutes);
}

// Функция для обработки изменения выбора типа срока для окна создания задачи
function handleDeadlineTypeChangeForCreate() {
    const deadlineType = document.getElementById('createDeadlineType').value;
    const createDeadlinesContainer = document.getElementById('createDeadlinesContainer');
    const createWeeklyDeadlineContainer = document.getElementById('createWeeklyDeadlineContainer');

    if (deadlineType === 'WEEKDAYS') {
        createDeadlinesContainer.style.display = 'none';
        createWeeklyDeadlineContainer.style.display = 'block';
    } else if (deadlineType === 'DATES') {
        createWeeklyDeadlineContainer.style.display = 'none';
        createDeadlinesContainer.style.display = 'block';
    }
}

// Добавляем слушатель события для изменения выбора типа срока для окна создания задачи
document.addEventListener('DOMContentLoaded', function () {
    const deadlineTypeDates = document.getElementById('createDeadlineTypeDates');
    deadlineTypeDates.addEventListener('change', handleDeadlineTypeChangeForCreate);
    handleDeadlineTypeChangeForCreate(); // Вызываем функцию при загрузке страницы для установки начального состояния
});

// Функция для обработки изменения выбора типа срока для окна изменения задачи
function handleDeadlineTypeChangeForUpdate() {
    const deadlineType = document.getElementById('updateDeadlineType').value;
    const updateDeadlinesContainer = document.getElementById('updateDeadlinesContainer');
    const updateWeeklyDeadlineContainer = document.getElementById('updateWeeklyDeadlineContainer');

    if (deadlineType === 'WEEKDAYS') {
        updateDeadlinesContainer.style.display = 'none';
        updateWeeklyDeadlineContainer.style.display = 'block';
    } else if (deadlineType === 'DATES') {
        updateWeeklyDeadlineContainer.style.display = 'none';
        updateDeadlinesContainer.style.display = 'block';
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const performersSelect = document.getElementById('performers');
    const selectedPerformersContainer = document.getElementById('selectedPerformersContainer');

    performersSelect.addEventListener('change', function () {
        updateSelectedPerformers();
    });

    function updateSelectedPerformers() {
        const selectedPerformers = Array.from(performersSelect.selectedOptions)
            .map(option => option.text);

        selectedPerformersContainer.textContent = selectedPerformers.join('\n');
    }
});

const updatePerformersSelect = document.getElementById('updatePerformers');
const selectedPerformersContainer = document.getElementById('updateSelectedPerformersContainer');

// Обновление списка исполнителей при изменении
updatePerformersSelect.addEventListener('change', updateSelectedPerformers);

// Обновление списка исполнителей при открытии модального окна
const updateTaskModal = document.getElementById('updateTaskModal');
if (updateTaskModal) {
    updateTaskModal.addEventListener('show.bs.modal', updateSelectedPerformers);
}

function updateSelectedPerformers() {
    selectedPerformersContainer.textContent = Array.from(updatePerformersSelect.selectedOptions)
        .map(option => option.text)
        .join('\n');
}

window.onload = function () {
    const message = document.getElementById('message');
    message.classList.remove('hidden'); // Удаление класса hidden для отображения элемента

    // Задержка перед скрытием сообщения (10 секунд)
    setTimeout(function () {
        message.classList.add('hidden'); // Добавление класса hidden для скрытия элемента
    }, 10000); // 10000 миллисекунд = 10 секунд
};

// document.addEventListener('DOMContentLoaded', function () {
//     const createTaskForm = document.querySelector('#createTaskModal form');
//
//     createTaskForm.addEventListener('submit', function (event) {
//         event.preventDefault(); // предотвращаем стандартное поведение формы
//
//         // Добавим вывод данных формы в консоль
//         console.log('Create Task Form Data:', new FormData(this));
//     });
// });

// document.addEventListener('DOMContentLoaded', function () {
//     const createTaskForm = document.querySelector('#updateTaskModal form');
//
//     createTaskForm.addEventListener('submit', function (event) {
//         event.preventDefault(); // предотвращаем стандартное поведение формы
//
//         // Добавим вывод данных формы в консоль
//         console.log('Update Task Form Data:', new FormData(this));
//     });
// });