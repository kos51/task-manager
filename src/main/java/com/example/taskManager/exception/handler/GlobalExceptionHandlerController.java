package com.example.taskManager.exception.handler;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public interface GlobalExceptionHandlerController {
    default void setErrorMessage(Exception e, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("infoMessage", e.getMessage());
    }
}
