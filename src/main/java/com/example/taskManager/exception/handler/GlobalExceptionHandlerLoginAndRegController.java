//package com.example.taskManager.exception.handler;
//
//import com.example.taskManager.exception.InvalidException;
//import com.example.taskManager.exception.NotFoundException;
//import com.example.taskManager.exception.ServiceException;
//import com.example.taskManager.web.controller.LoginAndRegController;
//import org.springframework.security.authentication.AuthenticationServiceException;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.servlet.mvc.support.RedirectAttributes;
//
//@ControllerAdvice(assignableTypes = LoginAndRegController.class)
//public class GlobalExceptionHandlerLoginAndRegController implements GlobalExceptionHandlerController {
//
//    @ExceptionHandler(ServiceException.class)
//    public String handleServiceException(ServiceException se, RedirectAttributes redirectAttributes) {
//        setErrorMessage(se, redirectAttributes);
//        return "redirect:/register";
//    }
//
//    @ExceptionHandler(NotFoundException.class)
//    public String handleNotFoundException(NotFoundException ne, RedirectAttributes redirectAttributes) {
//        setErrorMessage(ne, redirectAttributes);
//        return "redirect:/login";
//    }
//
//    @ExceptionHandler(InvalidException.class)
//    public String handleInvalidException(InvalidException ie, RedirectAttributes redirectAttributes) {
//        setErrorMessage(ie, redirectAttributes);
//        return "redirect:/login";
//    }
//}
