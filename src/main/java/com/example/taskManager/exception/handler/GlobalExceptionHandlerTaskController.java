package com.example.taskManager.exception.handler;

import com.example.taskManager.exception.InvalidException;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
public class GlobalExceptionHandlerTaskController implements GlobalExceptionHandlerController {

    @ExceptionHandler(InvalidException.class)
    public String handleInvalidException(InvalidException ie, RedirectAttributes redirectAttributes) {
        setErrorMessage(ie, redirectAttributes);
        return "redirect:/personalArea";
    }

    @ExceptionHandler(NotFoundException.class)
    public String handleNotFoundException(NotFoundException nfe, RedirectAttributes redirectAttributes) {
        setErrorMessage(nfe, redirectAttributes);
        return "redirect:/personalArea";
    }

    @ExceptionHandler(ServiceException.class)
    public String handleNotFoundException(ServiceException se, RedirectAttributes redirectAttributes) {
        setErrorMessage(se, redirectAttributes);
        return "redirect:/personalArea";
    }
}
