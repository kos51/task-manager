package com.example.taskManager.repository;

import com.example.taskManager.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM Client ORDER BY surname")
    List<Client> findAll();

    Client findByEmail(String email);

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN true ELSE false END FROM Client c WHERE c.id = :clientId AND c.telegramChatId IS NOT NULL")
    boolean existsChatIdByClientId(@Param("clientId") Long clientId);

    Client getClientByTelegramChatId(Long chatId);
}
