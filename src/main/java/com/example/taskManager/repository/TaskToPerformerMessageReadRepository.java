package com.example.taskManager.repository;

import com.example.taskManager.entity.TaskToPerformerMessageRead;
import com.example.taskManager.service.TaskToPerformerMessageReadService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TaskToPerformerMessageReadRepository extends JpaRepository<TaskToPerformerMessageRead, Long> {

    @Query("SELECT ttpmr FROM TaskToPerformerMessageRead  ttpmr" +
            " WHERE ttpmr.taskId =:taskId AND ttpmr.performerId =:performerId")
    TaskToPerformerMessageRead findByTaskIdAndPerformerId(@Param("taskId") Long taskId, @Param("performerId") Long performerId);

    @Query("SELECT ttpmr FROM TaskToPerformerMessageRead ttpmr WHERE ttpmr.taskId =:taskId")
    List<TaskToPerformerMessageRead> findByTaskId(@Param("taskId") Long taskId);

    @Query("SELECT ttpmr FROM TaskToPerformerMessageRead ttpmr WHERE ttpmr.performerId =:performerId")
    List<TaskToPerformerMessageRead> findByPerformerId(@Param("performerId") Long performerId);

    @Query("SELECT CASE WHEN COUNT(ttpmr) > 0 THEN true ELSE false END" +
            " FROM TaskToPerformerMessageRead ttpmr" +
            " WHERE ttpmr.taskId =:taskId AND ttpmr.performerId =:performerId")
    boolean existsByTaskIdAndPerformerId(@Param("taskId") Long taskId, @Param("performerId") Long performerId);

    @Modifying
    @Query("UPDATE TaskToPerformerMessageRead t SET t.isMessageRead = :messageRead WHERE t.taskId = :taskId AND t.performerId = :performerId")
    void updateMessageReadStatus(@Param("taskId") Long taskId, @Param("performerId") Long performerId, @Param("messageRead") boolean messageRead);
}
