package com.example.taskManager.repository;

import com.example.taskManager.entity.Task;
import com.example.taskManager.enums.DayOfWeek;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT DISTINCT t FROM Task t" +
            " JOIN FETCH t.performers" +
            " WHERE t.owner.id =:ownerId" +
            " ORDER BY t.id")
    List<Task> findAllByOwnerId(@Param("ownerId") Long ownerId);

    @Query("SELECT t FROM Task t" +
            " JOIN FETCH t.performers AS p" +
            " WHERE p.telegramChatId =:chatId" +
            " AND t.status = 'ACTIVE'" +
            "ORDER BY t.id")
    List<Task> findActiveTasksByTelegramChatId(@Param("chatId") Long chatId);

    @Query("SELECT t FROM Task t" +
            " JOIN FETCH t.performers AS p" +
            " WHERE p.telegramChatId =:chatId" +
            " AND t.status = 'CLOSED'" +
            "ORDER BY t.id")
    List<Task> findClosedTasksByTelegramChatId(@Param("chatId") Long chatId);

    @Query("SELECT DISTINCT t FROM Task t" +
            " JOIN FETCH t.performers" +
            " WHERE t.owner.telegramChatId =:chatId" +
            " AND t.status = 'ACTIVE'" +
            "ORDER BY t.id")
    List<Task> findMyCreatedAndActiveTasksByTelegramChatId(@Param("chatId") Long chatId);

    @Query("SELECT DISTINCT t FROM Task t" +
            " JOIN FETCH t.performers" +
            " LEFT JOIN FETCH t.deadlines" +
            " WHERE t.id =:id" +
            " AND t.owner.id =:ownerId " +
            "ORDER BY t.id")
    Task findByIdAndOwnerId(@Param("id") Long id, @Param("ownerId") Long ownerId);

    @Query("SELECT DISTINCT t FROM Task t" +
            " JOIN t.deadlines d" +
            " JOIN FETCH t.performers" +
            " WHERE d.executionDeadline =:deadline " +
            "ORDER BY t.id")
    List<Task> findAllByDeadline(@Param("deadline") LocalDateTime deadline);

    @Query("SELECT DISTINCT t FROM Task t" +
            " JOIN t.dayTimes dt" +
            " JOIN FETCH t.performers" +
            " WHERE dt.dayOfWeek =:dayOfWeek AND dt.time =:currentTime " +
            "ORDER BY t.id")
    List<Task> findAllByDayTime(@Param("dayOfWeek") DayOfWeek dayOfWeek, @Param("currentTime") LocalTime currentTime);

    @Query("SELECT DISTINCT t FROM Task t" +
            " JOIN t.deadlines d" +
            " JOIN FETCH t.performers" +
            " WHERE t.notificationType =:type " +
            "ORDER BY t.id")
    List<Task> findAllByNotificationType(@Param("type") String type);
}
