package com.example.taskManager.api;

import net.glxn.qrgen.QRCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QRCodeController {

    @GetMapping(value = "/qr-code/{clientId}", produces = "image/png")
    public ResponseEntity<byte[]> generateQRCode(@PathVariable String clientId) {
        byte[] qrCodeBytes = QRCode.from("https://t.me/ElectronicDiary_bot?start=" + clientId).stream().toByteArray();
        return ResponseEntity.ok().body(qrCodeBytes);
    }
}
