package com.example.taskManager.entity;

import com.example.taskManager.security.ClientRole;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "client")
@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String surname;

    @NotNull
    private String name;

    @NotNull
    private String patronymic;

    @NotNull
    private String email;

    @NotNull
    private String password;

    private Long telegramChatId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ClientRole role;

    @OneToOne(mappedBy = "client", cascade = CascadeType.ALL)
    private PasswordResetToken passwordResetToken;
}
