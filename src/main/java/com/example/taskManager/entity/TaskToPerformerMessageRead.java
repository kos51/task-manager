package com.example.taskManager.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Entity
@Table(name = "task_to_performer_message_read")
@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class TaskToPerformerMessageRead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "task_id")
    private Long taskId;

    @Column(name = "performer_id")
    private Long performerId;

    @Column(name = "is_message_read")
    private boolean isMessageRead;
}
