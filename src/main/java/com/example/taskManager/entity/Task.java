package com.example.taskManager.entity;

import com.example.taskManager.enums.StatusCode;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static com.example.taskManager.enums.StatusCode.ACTIVE;

@Entity
@Table(name = "task")
@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String taskName;
    private String comment;

    @Column(name = "created_date_time")
    private LocalDateTime createdDateTime;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StatusCode status;

    @ManyToMany
    @JoinTable(
            name = "task_to_performer",
            joinColumns = @JoinColumn(name = "task_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "performer_id", referencedColumnName = "id")
    )
    private Set<Client> performers = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    private Client owner;

    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Deadline> deadlines = new HashSet<>();

    @OneToMany(mappedBy = "task", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<DayTime> dayTimes = new HashSet<>();

    @Column(name = "notification_type")
    private String notificationType;

    public void addDayTime(DayTime dayTime) {
        dayTimes.add(dayTime);
        dayTime.setTask(this);
    }

    public boolean isActive() {
        return this.getStatus().equals(ACTIVE);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Task task = (Task) obj;
        return Objects.equals(id, task.id) &&
                Objects.equals(taskName, task.taskName) &&
                Objects.equals(comment, task.comment) &&
                Objects.equals(createdDateTime, task.createdDateTime) &&
                status == task.status &&
                Objects.equals(performers, task.performers) &&
                Objects.equals(notificationType, task.notificationType) &&
                Objects.equals(owner, task.owner) &&
                Objects.equals(deadlines, task.deadlines) &&
                Objects.equals(dayTimes, task.dayTimes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taskName, comment, createdDateTime, status, performers, notificationType, owner, deadlines, dayTimes);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", taskName='" + taskName + '\'' +
                ", comment='" + comment + '\'' +
                ", createdDate=" + createdDateTime +
                ", status=" + status +
                ", performers=" + performers +
                ", notificationType=" + notificationType +
                ", deadlines=" + deadlines +
                ", dayTimes=" + dayTimes +
                ", owner=" + owner +
                '}';
    }
}
