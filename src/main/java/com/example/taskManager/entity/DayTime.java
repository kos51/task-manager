package com.example.taskManager.entity;

import com.example.taskManager.enums.DayOfWeek;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.Objects;

@Entity
@Table(name = "day_time")
@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class DayTime {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private DayOfWeek dayOfWeek;

    @Column(nullable = false)
    private LocalTime time;

    @ManyToOne
    @JoinColumn(name = "task_id", referencedColumnName = "id")
    private Task task;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DayTime dayTime = (DayTime) o;
        return Objects.equals(id, dayTime.id) && dayOfWeek == dayTime.dayOfWeek && Objects.equals(time, dayTime.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dayOfWeek, time);
    }

    @Override
    public String toString() {
        return "DayTime{" +
                "id=" + id +
                ", dayOfWeek=" + dayOfWeek +
                ", time=" + time +
                '}';
    }
}
