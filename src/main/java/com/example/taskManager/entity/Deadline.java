package com.example.taskManager.entity;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "deadline")
@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class Deadline {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_time_of_execution")
    private LocalDateTime executionDeadline;

    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deadline deadline = (Deadline) o;
        return Objects.equals(id, deadline.id) && Objects.equals(executionDeadline, deadline.executionDeadline);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, executionDeadline);
    }

    @Override
    public String toString() {
        return "Deadline{" +
                "id=" + id +
                ", executionDeadLine=" + executionDeadline +
                '}';
    }
}
