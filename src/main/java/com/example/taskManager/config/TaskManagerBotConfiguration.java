package com.example.taskManager.config;

import com.example.taskManager.service.TaskManagerBotService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
public class TaskManagerBotConfiguration {

    @Bean
    public TelegramBotsApi telegramBotsApi(TaskManagerBotService taskManagerBotService) throws TelegramApiException {
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(taskManagerBotService);

        return telegramBotsApi;
    }
}
