package com.example.taskManager.web.controller;

import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.service.MailService;
import com.example.taskManager.service.PasswordResetTokenService;
import com.example.taskManager.service.dto.PasswordResetTokenDto;
import com.example.taskManager.web.form.EmailForm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class PasswordResetController {
    private final PasswordResetTokenService passwordResetTokenService;
    private final MailService mailService;

    @Value("${app.url}")
    private String appUrl;

    @GetMapping("/forgot-password")
    public String showForgotPasswordForm(Model model) {
        model.addAttribute("emailForm", new EmailForm());
        return "forgot-password-form";
    }

    @PostMapping("/forgot-password")
    public String processForgotPassword(@ModelAttribute("emailForm") @Valid EmailForm emailForm,
            BindingResult bindingResult,
            Model model) {
        if (bindingResult.hasErrors()) {
            return "forgot-password-form";
        }
        try {
            PasswordResetTokenDto resetTokenDto = passwordResetTokenService.requestPasswordReset(emailForm.getEmail());

            if (!appUrl.endsWith("/")) {
                appUrl += "/";
            }

            String resetUrl = appUrl + "reset-password?token=" + resetTokenDto.getToken();
            String subject = "Восстановление пароля";
            String message = "Для восстановления пароля перейдите по ссылке: <a href=\"" + resetUrl + "\">" + resetUrl + "</a>";
            mailService.sendHtmlEmail(emailForm.getEmail(), subject, message);
            model.addAttribute("infoMessage", "Ссылка отправлена на ваш email!");
        } catch (NotFoundException e) {
            model.addAttribute("infoMessage", e.getMessage());
        }
        return "login-form";
    }

    @GetMapping("/reset-password")
    public String showResetPasswordForm(@RequestParam(name = "token") String token, Model model) {
        model.addAttribute("token", token);
        return "reset-password-form";
    }

    @PostMapping("/reset-password")
    public String resetPassword(@RequestParam(name = "token") String token, @RequestParam(name = "password") String password, Model model) {
        try {
            passwordResetTokenService.resetPassword(token, password);
            model.addAttribute("infoMessage", "Пароль успешно изменен!");
        } catch (ServiceException e) {
            model.addAttribute("infoMessage", e.getMessage());
        }
        return "redirect:/login";
    }
}
