package com.example.taskManager.web.controller;

import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.web.form.AuthForm;
import com.example.taskManager.web.form.RegisterForm;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Controller
@RequiredArgsConstructor
public class LoginAndRegController {
    private final ClientService clientService;

    @GetMapping("/login")
    public String getLogin(@RequestParam(name = "expired", defaultValue = "false") String expired,
            @RequestParam(name = "error", required = false) String error,
            @NotNull Model model) {
        try {
            if ("true".equals(error)) {
                model.addAttribute("infoMessage", "Неверный логин или пароль!");
            }
            if ("true".equals(expired)) {
                model.addAttribute("infoMessage", "Превышен лимит одновременных сессий. Попробуйте снова позже");
            } else {
                model.addAttribute("sessionExpired", false)
                     .addAttribute("form", new AuthForm());
            }
            return "login-form";
        } catch (AuthenticationException e) {
            model.addAttribute("infoMessage", e.getMessage());
            return "login-form";
        }
    }

    @GetMapping("/register")
    public String getRegister(@NotNull @Valid Model model) {
        model.addAttribute("form", new RegisterForm());
        return "register";
    }

    @PostMapping("/register")
    public String postRegister(@Valid @ModelAttribute("form") @NotNull RegisterForm form,
            BindingResult result,
            Model model) {
        if (!result.hasErrors()) {
            ClientDto clientDto = clientService.register(form.getSurname(),
                                                         form.getName(),
                                                         form.getPatronymic(),
                                                         form.getEmail(),
                                                         form.getPassword(),
                                                         form.getRole());
            model.addAttribute("infoMessage", "Регистрация прошла успешно!")
                 .addAttribute("clientId", clientDto.getId());
            return "registration-success";
        }
        model.addAttribute("form", form);
        return "register";
    }
}