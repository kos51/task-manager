package com.example.taskManager.web.controller;

import com.example.taskManager.exception.InvalidException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RequiredArgsConstructor
@Controller
public class StartController {
    public static String INFO_MESSAGE = "";

    public static void checkRedirectedAttributes(Model model, RedirectAttributes redirectAttributes, String attribute) {
        if (redirectAttributes.containsAttribute(attribute)) {
            model.addAttribute("infoMessage", redirectAttributes.getFlashAttributes().get(attribute));
        }
    }

    public static void handleValidationErrors(BindingResult result) {
        if (result.hasErrors()) {
            INFO_MESSAGE = result.getAllErrors().toString();
            throw new InvalidException("Ошибка валидации: введено неверное значение! Повторите попытку!");
        }
    }

    @GetMapping("/")
    public String getStart(Model model, RedirectAttributes redirectAttributes) {
        checkRedirectedAttributes(model, redirectAttributes, "infoMessage");
        try {
            return "start";
        } catch (Throwable e) {
            redirectAttributes.addFlashAttribute("infoMessage", e.getMessage());
            return "redirect:/";
        }
    }
}
