package com.example.taskManager.web.controller;

import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.TaskServiceImpl;
import com.example.taskManager.service.dto.TaskDto;
import com.example.taskManager.web.form.TaskCreateForm;
import com.example.taskManager.web.form.TaskUpdateForm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static com.example.taskManager.web.controller.StartController.handleValidationErrors;

@Controller
@RequestMapping("/personalArea/task/")
@RequiredArgsConstructor
public class TaskController {
    private final ClientService clientService;
    private final TaskServiceImpl taskServiceImpl;

    @PostMapping("/create")
    public String createTask(@ModelAttribute("form") @NotNull @Valid TaskCreateForm form,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            @RequestParam(name = "deadlineType", required = false) String deadlineType,
            @RequestParam(name = "messageReadStatus", required = false) boolean messageReadStatus) {
        handleValidationErrors(result);

        if ("WEEKDAYS".equals(deadlineType)) {
            taskServiceImpl.createTaskWithWeekdays(form.getTaskName(),
                                                   form.getComment(),
                                                   form.getPerformers(),
                                                   form.getWeeklyDay(),
                                                   form.getWeeklyTime(),
                                                   form.getNotificationType(),
                                                   messageReadStatus,
                                                   clientService.currentClientDto().getId());
        } else if ("DATES".equals(deadlineType)) {
            taskServiceImpl.createTaskWithDeadlines(form.getTaskName(),
                                                    form.getComment(),
                                                    form.getPerformers(),
                                                    form.getDeadlines(),
                                                    form.getNotificationType(),
                                                    messageReadStatus,
                                                    clientService.currentClientDto().getId());
        }

        redirectAttributes.addFlashAttribute("infoMessage", "Задача успешно создана!");
        return "redirect:/personalArea";
    }

    @PostMapping("/update")
    public String updateTask(@ModelAttribute("form") @NotNull @Valid TaskUpdateForm form,
            BindingResult result,
            RedirectAttributes redirectAttributes,
            @RequestParam(name = "deadlineType", required = false) String deadlineType) {
        handleValidationErrors(result);

        Long ownerId = clientService.currentClientDto().getId();
        TaskDto taskDto = taskServiceImpl.findByIdAndOwnerId(form.getTaskId(), ownerId);

        if ("WEEKDAYS".equals(deadlineType)) {
            taskServiceImpl.updateTaskWithWeekDays(taskDto.getId(),
                                                   form.getTaskName(),
                                                   form.getComment(),
                                                   form.getStatus(),
                                                   form.getUpdatePerformers(),
                                                   form.getNotificationType(),
                                                   form.getWeeklyDay(),
                                                   form.getWeeklyTime(),
                                                   ownerId);
        } else if ("DATES".equals(deadlineType)) {
            taskServiceImpl.updateTaskWithDeadlines(taskDto.getId(),
                                                    form.getTaskName(),
                                                    form.getComment(),
                                                    form.getStatus(),
                                                    form.getUpdatePerformers(),
                                                    form.getNotificationType(),
                                                    form.getUpdateDeadlines(),
                                                    ownerId);
        }
        redirectAttributes.addFlashAttribute("infoMessage", "Задача успешно обновлена!");
        return "redirect:/personalArea";
    }

    @PostMapping("/delete/{taskId}")
    public String deleteTask(@PathVariable Long taskId, RedirectAttributes redirectAttributes) {
        taskServiceImpl.deleteTask(taskId, clientService.currentClientDto().getId());
        redirectAttributes.addFlashAttribute("infoMessage", "Задача успешно удалена!");
        return "redirect:/personalArea";
    }
}
