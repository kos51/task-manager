package com.example.taskManager.web.controller;

import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.TaskServiceImpl;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.taskManager.web.controller.StartController.checkRedirectedAttributes;

@Controller
@RequiredArgsConstructor
public class PersonalAreaController {
    private final TaskServiceImpl taskServiceImpl;
    private final ClientService clientService;

    @GetMapping("/personalArea")
    public String getPersonalArea(Model model, RedirectAttributes redirectAttributes) {
        checkRedirectedAttributes(model, redirectAttributes, "infoMessage");

        ClientDto clientDto = clientService.currentClientDto();
        if (clientDto == null) {
            model.addAttribute("infoMessage", "Сначала необходимо авторизоваться!");
            return "login-form";
        }

        List<TaskDto> taskDtoList = taskServiceImpl.findAllByOwnerId(clientDto.getId());
        Map<Long, Map<Long, Boolean>> taskMessageReadStatusMap = taskServiceImpl.getTaskMessageReadStatus(taskDtoList);

        model.addAttribute("name", clientDto.getName())
             .addAttribute("tasks", taskDtoList)
             .addAttribute("taskMessageReadStatusMap", mapToMapStringConverter(taskMessageReadStatusMap))
             .addAttribute("users", clientService.findAll());
        return "personalArea";
    }

    private Map<String, Map<String, Boolean>> mapToMapStringConverter(Map<Long, Map<Long, Boolean>> taskMessageReadStatusMap) {
        Map<String, Map<String, Boolean>> stringKeyedTasksMessageReadStatusMap = new HashMap<>();
        for (Map.Entry<Long, Map<Long, Boolean>> entry : taskMessageReadStatusMap.entrySet()) {
            Map<String, Boolean> innerMap = new HashMap<>();
            for (Map.Entry<Long, Boolean> innerEntry : entry.getValue().entrySet()) {
                innerMap.put(innerEntry.getKey().toString(), innerEntry.getValue());
            }
            stringKeyedTasksMessageReadStatusMap.put(entry.getKey().toString(), innerMap);
        }
        return stringKeyedTasksMessageReadStatusMap;
    }
}
