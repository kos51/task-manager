package com.example.taskManager.web.form;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@RequiredArgsConstructor
@Data
@Accessors(chain = true)
public class AuthForm {

    @Email
    @NotNull
    private String email;

    @NotNull(message = "Требуется ввести пароль")
    @Pattern(regexp = "^\\S{5,}$", message = "Пароль должен быть не менее 5 символов в длину")
    private String password;
}
