package com.example.taskManager.web.form;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@RequiredArgsConstructor
@Accessors(chain = true)
public class RegisterForm {

    @NotNull(message = "Требуется ввести фамилию")
    @Pattern(regexp = "^[А-ЯЁ][а-яё]+$", message = "Поле должно содержать только символы Кирилицы, начинаться с заглавной буквы")
    private String surname;

    @NotNull(message = "Требуется ввести имя")
    @Pattern(regexp = "^[А-ЯЁ][а-яё]+$", message = "Поле должно содержать только символы Кирилицы, начинаться с заглавной буквы")
    private String name;

    @NotNull(message = "Требуется ввести отчество")
    @Pattern(regexp = "^[А-ЯЁ][а-яё]+$", message = "Поле должно содержать только символы Кирилицы, начинаться с заглавной буквы")
    private String patronymic;

    @Email(message = "Неверный формат")
    @NotNull(message = "Требуется ввести email")
    private String email;

    @NotNull(message = "Требуется ввести пароль")
    @Pattern(regexp = "^\\S{5,}$", message = "Пароль должен быть не менее 5 символов в длину")
    private String password;

    @NotNull(message = "Требуется указать роль из списка")
    private String role;
}
