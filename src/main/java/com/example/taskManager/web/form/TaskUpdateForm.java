package com.example.taskManager.web.form;


import com.example.taskManager.entity.Client;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@Data
@Accessors(chain = true)
public class TaskUpdateForm {

    @NotNull
    private Long taskId;

    @NotNull(message = "Требуется ввести название или номер задачи")
    private String taskName;

    private String comment;

    @NotNull
    @Pattern(regexp = "на исполнении|завершено", message = "Статус должен быть 'на исполнении' или 'завершено'")
    private String status;

    @NotEmpty(message = "Необходимо указать исполнителей")
    private Set<Client> updatePerformers;

    @NotEmpty(message = "Необходимо указать сроки исполнения")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private List<String> updateDeadlines;

    @NotNull
    private String weeklyDay;

    @NotNull
    private String weeklyTime;

    private String notificationType;
}
