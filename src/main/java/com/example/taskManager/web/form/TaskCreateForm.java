package com.example.taskManager.web.form;

import com.example.taskManager.entity.Client;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
@Data
@Accessors(chain = true)
public class TaskCreateForm {

    @NotNull(message = "Требуется ввести название или номер задачи")
    private String taskName;
    private String comment;

    @NotEmpty(message = "Необходимо указать исполнителей")
    private Set<Client> performers;

    @NotEmpty(message = "Необходимо указать сроки исполнения")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    //todo - в ftlh приходит строка "yyyy-MM-ddTHH:mm" надо сконвертировать ее в LocalDateTime и положить в task. и для taskDto тоже
    private List<String> deadlines;

    @NotNull
    private String weeklyDay;

    @NotNull
    private String weeklyTime;

    private String notificationType;
}
