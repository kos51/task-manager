package com.example.taskManager.security;

import com.example.taskManager.entity.Client;
import com.example.taskManager.repository.ClientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomDetailsService implements UserDetailsService {
    private final ClientRepository clientRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Client client = clientRepository.findByEmail(email);
        if (client == null) {
            throw new UsernameNotFoundException("Пользователь с email: " + email + " не найден!");
        }
        return new CustomUserDetails(client.getId(),
                                     client.getSurname(),
                                     client.getName(),
                                     client.getPatronymic(),
                                     client.getEmail(),
                                     client.getPassword(),
                                     client.getTelegramChatId(),
                                     new CustomGrantedAuthority(client.getRole()));
    }
}