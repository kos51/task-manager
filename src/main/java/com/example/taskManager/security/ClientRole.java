package com.example.taskManager.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum ClientRole {
    SUPERVISOR,
    PERFORMER;
}
