package com.example.taskManager.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Objects;

@RequiredArgsConstructor
public class CustomGrantedAuthority implements GrantedAuthority {
    private final ClientRole clientRole;

    @Override
    public String getAuthority() {
        return clientRole.name();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomGrantedAuthority that = (CustomGrantedAuthority) o;
        return clientRole == that.clientRole;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientRole);
    }
}
