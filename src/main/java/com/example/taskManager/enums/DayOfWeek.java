package com.example.taskManager.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Getter
public enum DayOfWeek {
    MONDAY("по понедельникам"),
    TUESDAY("по вторникам"),
    WEDNESDAY("по средам"),
    THURSDAY("по четвергам"),
    FRIDAY("по пятницам"),
    SATURDAY("по субботам"),
    SUNDAY("по воскресеньям");
    private static final Map<java.time.DayOfWeek, DayOfWeek> DAY_OF_WEEK_MAP = new HashMap<>();


    static {
        for (DayOfWeek enumDayOfWeek : values()) {
            DAY_OF_WEEK_MAP.put(getJavaTimeDayOfWeek(enumDayOfWeek), enumDayOfWeek);
        }
    }

    private final String message;

    private static java.time.DayOfWeek getJavaTimeDayOfWeek(DayOfWeek dayOfWeek) {
        switch (dayOfWeek) {
            case MONDAY:
                return java.time.DayOfWeek.MONDAY;
            case TUESDAY:
                return java.time.DayOfWeek.TUESDAY;
            case WEDNESDAY:
                return java.time.DayOfWeek.WEDNESDAY;
            case THURSDAY:
                return java.time.DayOfWeek.THURSDAY;
            case FRIDAY:
                return java.time.DayOfWeek.FRIDAY;
            case SATURDAY:
                return java.time.DayOfWeek.SATURDAY;
            case SUNDAY:
                return java.time.DayOfWeek.SUNDAY;
            default:
                throw new IllegalArgumentException("Unsupported DayOfWeek: " + dayOfWeek);
        }
    }

    public static DayOfWeek convertToEnumDayOfWeek(java.time.DayOfWeek dayOfWeek) {
        return DAY_OF_WEEK_MAP.get(dayOfWeek);
    }

    public static java.time.DayOfWeek convertToJavaTimeDayOfWeek(DayOfWeek dayOfWeek) {
        return getJavaTimeDayOfWeek(dayOfWeek);
    }
}
