package com.example.taskManager.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum StatusCode {
    ACTIVE("на исполнении"),
    CLOSED("завершено");
    private final String message;

    public static StatusCode fromMessage(String message) {
        for (StatusCode statusCode : values()) {
            if (statusCode.message.equals(message)) {
                return statusCode;
            }
        }
        throw new IllegalArgumentException("Указан неверный статус: " + message);
    }
}
