package com.example.taskManager.service;

import com.example.taskManager.service.dto.PasswordResetTokenDto;

public interface PasswordResetTokenService {
    PasswordResetTokenDto requestPasswordReset(String email);

    void resetPassword(String token, String newPassword);
}
