package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.entity.PasswordResetToken;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.repository.PasswordResetTokenRepository;
import com.example.taskManager.service.dto.PasswordResetTokenDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PasswordResetTokenServiceImpl implements PasswordResetTokenService {
    private final ClientRepository clientRepository;
    private final PasswordResetTokenRepository passwordResetTokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final Converter<PasswordResetToken, PasswordResetTokenDto> passwordResetTokenDtoConverter;

    @Override
    public PasswordResetTokenDto requestPasswordReset(String email) {
        Client client = clientRepository.findByEmail(email);
        if (client == null) {
            throw new NotFoundException("Пользователь с email: " + email + " не найден!");
        }

        PasswordResetToken resetToken = new PasswordResetToken().setToken(UUID.randomUUID().toString())
                                                                .setClient(client)
                                                                .setExpireDate(LocalDateTime.now().plusHours(1));
        passwordResetTokenRepository.save(resetToken);
        return passwordResetTokenDtoConverter.convert(resetToken);
    }

    @Override
    @Transactional
    public void resetPassword(String token, String newPassword) {
        PasswordResetToken resetToken = passwordResetTokenRepository.findByToken(token);

        if (resetToken == null || resetToken.getExpireDate().isBefore(LocalDateTime.now())) {
            throw new ServiceException("Недействительный или истекший токен");
        }

       Client client = resetToken.getClient();
       client.setPassword(passwordEncoder.encode(newPassword));
       clientRepository.save(client);

        passwordResetTokenRepository.deleteByTokenId(resetToken.getId());
    }
}
