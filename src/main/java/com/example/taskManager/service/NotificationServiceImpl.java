package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.entity.Deadline;
import com.example.taskManager.entity.Task;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;

import static com.example.taskManager.enums.DayOfWeek.convertToEnumDayOfWeek;
import static com.example.taskManager.enums.DayOfWeek.convertToJavaTimeDayOfWeek;

@Service
@Slf4j
@Data
@Accessors(chain = true)
@RequiredArgsConstructor
public class NotificationServiceImpl {
    private static final String TASK_DEADLINES_CHECK_ERROR = "Произошла ошибка при проверке сроков задачи: ";
    private static final String NOTIFICATION_WRITE_ERROR = "Ошибка записи типа уведомления в задачу!";
    private static final String NOTIFICATION_TITLE_DAY_TO_DEADLINE = "Срок истекает через сутки! " + "⏰";
    private static final String NOTIFICATION_TITLE_HOUR_TO_DEADLINE = "Срок истекает через час! " + "⏰";
    private static final String NOTIFICATION_TYPE_HOUR = "HOUR";
    private static final String NOTIFICATION_TYPE_DAY = "DAY";
    private final TaskManagerBotService taskManagerBotService;
    private final TaskService taskService;
    private final Converter<Task, TaskDto> taskTaskDtoConverter;
    private final Converter<Client, ClientDto> clientClientDtoConverter;
    private LocalDateTime currentLocalDateTime;

    @Scheduled(cron = "0 * * * * *")
    public void checkTaskDeadlines() {
        try {
            currentLocalDateTime = LocalDateTime.now().withSecond(0).withNano(0);
            LocalDateTime currentLocalDateTimePlusHour = currentLocalDateTime.plusHours(1);
            LocalDateTime currentLocalDateTimePlusDay = currentLocalDateTime.plusDays(1);

            List<Task> taskListNotificationHour = taskService.getAllTasksByNotificationType(NOTIFICATION_TYPE_HOUR);
            if (!taskListNotificationHour.isEmpty()) {
                sendNotificationsByLocalDayTime(currentLocalDateTimePlusHour, taskListNotificationHour);
            }
            List<Task> taskListNotificationDay = taskService.getAllTasksByNotificationType(NOTIFICATION_TYPE_DAY);
            if (!taskListNotificationDay.isEmpty()) {
                sendNotificationsByLocalDayTime(currentLocalDateTimePlusDay, taskListNotificationDay);
            }
        } catch (Exception e) {
            log.error(TASK_DEADLINES_CHECK_ERROR, e);
        }
    }

    @Scheduled(cron = "0 * * * * *")
    public void checkTaskDayTime() {
        currentLocalDateTime = LocalDateTime.now().withSecond(0).withNano(0);
        try {
            java.time.DayOfWeek currentDayOfWeek = currentLocalDateTime.getDayOfWeek();
            java.time.DayOfWeek theNextDayOfWeek = currentLocalDateTime.plusDays(1).getDayOfWeek();

            LocalTime currentTime = currentLocalDateTime.toLocalTime();
            LocalTime currentTimePlusHour = currentTime.plusHours(1);

            sendNotificationsByDayOfWeekAndLocalTime(theNextDayOfWeek, currentTime);
            sendNotificationsByDayOfWeekAndLocalTime(currentDayOfWeek, currentTimePlusHour);
        } catch (Exception e) {
            log.error(TASK_DEADLINES_CHECK_ERROR, e);
        }
    }

    private void sendNotificationsByLocalDayTime(LocalDateTime localDateTime, List<Task> taskList) {
        taskList.stream()
                .filter(Task::isActive)
                .flatMap(task -> task.getDeadlines().stream()
                                     .filter(deadline -> deadline.getExecutionDeadline().isEqual(localDateTime))
                                     .sorted(Comparator.comparing(Deadline::getExecutionDeadline)))
                .forEach(deadline -> {
                    Task task = deadline.getTask();
                    String notificationTitle = getNotificationTitle(task.getNotificationType());
                    sendNotification(task, notificationTitle);
                });
    }

    private String getNotificationTitle(String notificationType) {
        return NOTIFICATION_TYPE_HOUR.equals(notificationType) ? NOTIFICATION_TITLE_HOUR_TO_DEADLINE : NOTIFICATION_TITLE_DAY_TO_DEADLINE;
    }

    private void sendNotificationsByDayOfWeekAndLocalTime(java.time.DayOfWeek dayOfWeek, LocalTime localTime) {
        List<Task> taskList = taskService.getAllTasksByDayTime(convertToEnumDayOfWeek(dayOfWeek), localTime);

        taskList.stream()
                .filter(task -> task != null && task.isActive() && task.getNotificationType() != null)
                .flatMap(task -> task.getDayTimes().stream())
                .filter(dayTime -> {
                    Task task = dayTime.getTask();
                    DayOfWeek taskDayOfWeek = convertToJavaTimeDayOfWeek(dayTime.getDayOfWeek());
                    return (task.getNotificationType().equals(NOTIFICATION_TYPE_HOUR) &&
                            ((taskDayOfWeek.equals(dayOfWeek) || taskDayOfWeek.equals(dayOfWeek.plus(1))) && dayTime.getTime().equals(localTime)))
                            || (task.getNotificationType().equals(NOTIFICATION_TYPE_DAY) && taskDayOfWeek.equals(dayOfWeek) && dayTime.getTime().equals(localTime));
                })
                .forEach(dayTime -> {
                    Task task = dayTime.getTask();
                    String notificationTitle = getNotificationTitle(task.getNotificationType());
                    sendNotification(task, notificationTitle);
                });
    }

    private void sendNotification(Task task, String notificationTitle) {
        TaskDto taskDto = taskTaskDtoConverter.convert(task);
        if (taskDto != null) {
            taskManagerBotService.sendTelegramNotifications(task.getPerformers(), taskDto, notificationTitle, false);
        }
    }
}
