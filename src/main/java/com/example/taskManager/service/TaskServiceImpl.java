package com.example.taskManager.service;

import com.example.taskManager.entity.*;
import com.example.taskManager.enums.DayOfWeek;
import com.example.taskManager.enums.StatusCode;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.repository.TaskRepository;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.example.taskManager.enums.StatusCode.ACTIVE;
import static com.example.taskManager.enums.StatusCode.CLOSED;
import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private static final String TASK_CREATE_ERROR = "Произошла ошибка при создании задачи";
    private static final String TASK_UPDATE_ERROR = "Произошла ошибка при обновлении задачи";
    private static final String TASK_DELETE_ERROR = "Произошла ошибка при удалении задачи";
    private static final String taskWithNameAlreadyExist = "задача с указанным именем уже существует!";
    private static final String clientNotFound = "пользователь не найден!";
    private final TaskManagerBotService taskManagerBotService;
    private final TaskToPerformerMessageReadService taskToPerformerMessageReadService;
    private final ClientRepository clientRepository;
    private final TaskRepository taskRepository;
    private final Converter<Task, TaskDto> taskToTaskDtoConverter;

    private static Set<DayTime> getDayTimes(String weeklyDay, String weeklyTime) {
        LocalTime time = LocalTime.parse(weeklyTime);
        DayOfWeek dayOfWeek = DayOfWeek.valueOf(weeklyDay.toUpperCase());
        DayTime dayTime = new DayTime().setDayOfWeek(dayOfWeek)
                                       .setTime(time);

        Set<DayTime> dayTimes = new HashSet<>();
        dayTimes.add(dayTime);
        return dayTimes;
    }

    @Override
    public List<TaskDto> findActiveTasksByTelegramChatId(Long chatId) {
        return getAllTaskDto(taskRepository.findActiveTasksByTelegramChatId(chatId));
    }

    @Override
    public List<TaskDto> findClosedTasksByTelegramChatId(Long chatId) {
        return getAllTaskDto(taskRepository.findClosedTasksByTelegramChatId(chatId));
    }

    @Override
    public List<TaskDto> findMyCreatedAndActiveTasksByTelegramChatId(Long chatId) {
        return getAllTaskDto(taskRepository.findMyCreatedAndActiveTasksByTelegramChatId(chatId));
    }

    @Override
    public List<Task> getAllTasksByDeadline(LocalDateTime deadline) {
        return taskRepository.findAllByDeadline(deadline);
    }

    @Override
    public List<Task> getAllTasksByDayTime(DayOfWeek dayOfWeek, LocalTime currentTime) {
        return taskRepository.findAllByDayTime(dayOfWeek, currentTime);
    }

    @Override
    public List<Task> getAllTasksByNotificationType(String notificationType) {
        return taskRepository.findAllByNotificationType(notificationType);
    }

    public List<TaskDto> findAllByOwnerId(Long ownerId) {
        return taskRepository.findAllByOwnerId(ownerId)
                             .stream()
                             .map(taskToTaskDtoConverter::convert).filter(Objects::nonNull)
                             .peek(taskDto -> {
                                 if (taskDto.isDeadlineTask()) {
                                     taskDto.setDeadlines(sortDeadlines(taskDto.getDeadlines()));
                                 }
                             })
                             .collect(Collectors.toList());
    }

    List<String> sortDeadlines(List<String> deadlines) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy 'до' HH:mm");

        List<LocalDateTime> sortedDates = deadlines.stream()
                                                   .map(date -> LocalDateTime.parse(date, formatter))
                                                   .sorted()
                                                   .collect(Collectors.toList());

        return sortedDates.stream()
                          .map(formatter::format)
                          .collect(Collectors.toList());
    }

    public TaskDto findByIdAndOwnerId(Long taskId, Long ownerId) {
        Task task = taskRepository.findByIdAndOwnerId(taskId, ownerId);

        if (task != null) {
            return taskToTaskDtoConverter.convert(task);
        } else {
            throw new NotFoundException(String.format("Задача с номером: %s не найдена!", taskId));
        }
    }

    public void createTaskWithDeadlines(String taskName, String comment, Set<Client> performers, List<String> deadlines, String notificationType, boolean messageReadStatus, Long ownerId) {
        isValid(taskName, performers, ownerId);
        try {
            Client owner = getClient(ownerId);
            checkPerformersExists(performers);

            Task task = new Task().setTaskName(taskName)
                                  .setComment(comment)
                                  .setCreatedDateTime(LocalDateTime.now())
                                  .setStatus(ACTIVE)
                                  .setOwner(owner)
                                  .setPerformers(performers)
                                  .setNotificationType(notificationType);
            Set<Deadline> deadlineEntities = new HashSet<>();
            for (String deadline : deadlines) {
                deadlineEntities.add(new Deadline().setExecutionDeadline(convertStringToLocalDateTime(deadline)).setTask(task));
            }
            task.setDeadlines(deadlineEntities);
            taskRepository.save(task);

            createReadMessageMarks(messageReadStatus, task);
            sendMessage(performers, task, messageReadStatus);
        } catch (NotFoundException | IllegalArgumentException e) {
            throw new ServiceException(e.getMessage());
        } catch (DataIntegrityViolationException e) {
            throw new ServiceException(String.format("%s: %s", TASK_CREATE_ERROR, taskWithNameAlreadyExist));
        } catch (Exception e) {
            log.error(TASK_CREATE_ERROR, e);
            throw new ServiceException(String.format("%s: %s", TASK_CREATE_ERROR, e.getMessage()));
        }
    }

    public void createTaskWithWeekdays(String taskName, String comment, Set<Client> performers, String weeklyDay, String weeklyTime, String notificationType, boolean messageReadStatus, Long ownerId) {
        try {
            isValid(taskName, performers, ownerId);

            Client owner = getClient(ownerId);
            checkPerformersExists(performers);

            Set<DayTime> dayTimes = getDayTimes(weeklyDay, weeklyTime);

            Task task = new Task().setTaskName(taskName)
                                  .setComment(comment)
                                  .setCreatedDateTime(LocalDateTime.now())
                                  .setStatus(ACTIVE)
                                  .setOwner(owner)
                                  .setPerformers(performers)
                                  .setNotificationType(notificationType);
            for (DayTime dayTime : dayTimes) {
                task.addDayTime(dayTime);
            }
            taskRepository.save(task);

            createReadMessageMarks(messageReadStatus, task);
            sendMessage(performers, task, messageReadStatus);
        } catch (NotFoundException e) {
            throw new ServiceException(e.getMessage());
        } catch (DataIntegrityViolationException e) {
            throw new ServiceException(String.format("%s: %s", TASK_CREATE_ERROR, taskWithNameAlreadyExist));
        } catch (Exception e) {
            log.error(TASK_CREATE_ERROR, e);
            throw new ServiceException(String.format("%s: %s", TASK_CREATE_ERROR, e.getMessage()));
        }
    }

    Client getClient(Long ownerId) {
        return clientRepository.findById(ownerId).orElseThrow(() -> new NotFoundException(String.format("%s: %s", TASK_CREATE_ERROR, clientNotFound)));
    }

    private void createReadMessageMarks(boolean messageReadStatus, Task task) {
        if (messageReadStatus) {
            taskToPerformerMessageReadService.createTaskToPerformerMessageReadRecords(task.getId(),
                                                                                      task.getPerformers().stream()
                                                                                          .map(Client::getId)
                                                                                          .collect(toList()));
        }
    }

    private void sendMessage(Set<Client> performers, Task task, boolean messageReadStatus) { //todo - может добавить read в DTO?
        TaskDto taskDto = taskToTaskDtoConverter.convert(task);
        if (taskDto != null) {
            if (taskDto.getDeadlines() != null && !taskDto.getDeadlines().isEmpty()) {
                taskDto.setDeadlines(sortDeadlines(taskDto.getDeadlines()));
            }
            taskManagerBotService.sendTelegramNotifications(performers, taskDto, "У вас новая задача!" + " \uD83D\uDFE2", messageReadStatus);
        }
    }

    @Transactional
    public void updateTaskWithDeadlines(Long taskId, String taskName, String comment, String status, Set<Client> performers, String notificationType, List<String> deadlines, Long ownerId) {
        updateTask(taskId, taskName, comment, status, performers, notificationType, ownerId, (task) -> {
            task.getDeadlines().clear();
            task.getDeadlines().addAll(updateDeadlines(deadlines, task));
        });
    }

    @Transactional
    public void updateTaskWithWeekDays(Long taskId, String taskName, String comment, String status, Set<Client> performers, String notificationType, String weeklyDay, String weeklyTime, Long ownerId) {
        updateTask(taskId, taskName, comment, status, performers, notificationType, ownerId, (task) -> updateWeekDays(weeklyDay, weeklyTime, task));
    }

    private void updateTask(Long taskId, String taskName, String comment, String status, Set<Client> performers, String notificationType, Long ownerId, Consumer<Task> updateMethod) {
        String title = "Задача обновлена" + " ❗";
        Task task = taskRepository.findByIdAndOwnerId(taskId, ownerId);
        checkTaskIsExist(task, TASK_UPDATE_ERROR);

        Task previousTask = new Task().setTaskName(task.getTaskName())
                                      .setComment(task.getComment())
                                      .setStatus(task.getStatus())
                                      .setNotificationType(task.getNotificationType())
                                      .setPerformers(task.getPerformers())
                                      .setDayTimes(new HashSet<>(task.getDayTimes()))
                                      .setDeadlines(new HashSet<>(task.getDeadlines()))
                                      .setOwner(task.getOwner());

        try {
            task.getDeadlines().clear();
            task.getDayTimes().clear();
            taskRepository.saveAndFlush(task);

            task.setId(taskId)
                .setTaskName(taskName)
                .setComment(comment)
                .setPerformers(performers)
                .setNotificationType(notificationType);

            String previousStatus = task.getStatus().getMessage();
            if (!previousStatus.equals(status)) {
                title = "Новый статус: " + status + " ❗";
                task.setStatus(StatusCode.fromMessage(status));
            }

            updateMethod.accept(task);
            taskRepository.saveAndFlush(task);

            taskToPerformerMessageReadService.updateMessageReadStatus(task.getId(),
                                                                      performers.stream()
                                                                                .map(Client::getId)
                                                                                .collect(toList()),
                                                                      false);

            TaskDto taskDto = taskToTaskDtoConverter.convert(task);
            if (taskDto != null) {
                if (taskDto.getDeadlines() != null && !taskDto.getDeadlines().isEmpty()) {
                    taskDto.setDeadlines(sortDeadlines(taskDto.getDeadlines()));
                }
                if (hasTaskChanged(previousTask, task)
                        && (ACTIVE.getMessage().equals(status) || (CLOSED.getMessage().equals(status) && !CLOSED.getMessage().equals(previousStatus)))) {
                    taskManagerBotService.sendTelegramNotifications(performers, taskDto, title, false);
                }
            }
        } catch (Exception e) {
            log.error(TASK_UPDATE_ERROR, e);
            throw new ServiceException(String.format("%s: %s", TASK_UPDATE_ERROR, e.getMessage()));
        }
    }

    private void updateWeekDays(String weeklyDay, String weeklyTime, Task task) {
        Set<DayTime> newDayTimes = getDayTimes(weeklyDay, weeklyTime);

        for (DayTime newDayTime : newDayTimes) {
            task.addDayTime(newDayTime);
        }
    }

    public void deleteTask(Long taskId, Long ownerId) {
        Task task = taskRepository.findByIdAndOwnerId(taskId, ownerId);

        checkTaskIsExist(task, TASK_DELETE_ERROR);
        isTaskBelongsToOwner(task, ownerId);

        try {
            TaskDto taskDto = taskToTaskDtoConverter.convert(task);
            if (taskDto != null) {
                if (taskDto.getDeadlines() != null && !taskDto.getDeadlines().isEmpty()) {
                    taskDto.setDeadlines(sortDeadlines(taskDto.getDeadlines()));
                }
                Set<Client> performers = task.getPerformers();
                taskRepository.delete(task);

                if (taskDto.getStatus().equals(ACTIVE.getMessage())) {
                    taskManagerBotService.sendTelegramNotifications(performers, taskDto, "Задача удалена!" + " \uD83D\uDD34", false);
                }
            }
        } catch (Exception e) {
            log.error(TASK_DELETE_ERROR, e);
            throw new ServiceException(String.format("%s: %s", TASK_DELETE_ERROR, e.getMessage()));
        }
    }

    List<TaskDto> getAllTaskDto(List<Task> tasks) {
        return tasks.stream()
                    .map(taskToTaskDtoConverter::convert).filter(Objects::nonNull)
                    .peek(taskDto -> {
                        if (taskDto.isDeadlineTask()) {
                            taskDto.setDeadlines(sortDeadlines(taskDto.getDeadlines()));
                        }
                    })
                    .collect(toList());
    }

    private Set<Deadline> updateDeadlines(List<String> newDeadlines, Task task) {
        Set<Deadline> deadlines = new HashSet<>();

        for (String newDeadline : newDeadlines) {
            Deadline deadline = new Deadline().setExecutionDeadline(convertStringToLocalDateTime(newDeadline)).setTask(task);
            deadlines.add(deadline);
        }
        return deadlines;
    }

    LocalDateTime convertStringToLocalDateTime(String dateString) {
        return dateString != null ? LocalDateTime.parse(dateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME) : null;
    }

    void isTaskBelongsToOwner(Task task, Long ownerId) {
        if (!task.getOwner().getId().equals(ownerId)) {
            throw new AccessDeniedException("У вас нет прав на изменение этой задачи!");
        }
    }

    void checkTaskIsExist(Task task, String errorCode) {
        if (task == null) {
            throw new NotFoundException(String.format("%s задача не найдена!", errorCode));
        }
    }

    void isValid(String taskName, Set<Client> performers, Long ownerId) {
        if (taskName == null || taskName.isEmpty() || ownerId == null || performers.isEmpty()) {
            throw new IllegalArgumentException(String.format("%s не все данные для создания задачи были предоставлены!", TASK_CREATE_ERROR));
        }
    }

    void checkPerformersExists(Set<Client> performers) {
        performers.forEach(performer -> clientRepository.findById(performer.getId())
                                                        .orElseThrow(() -> new EntityNotFoundException(String.format("%s: исполнитель с ID %s не найден!", TASK_CREATE_ERROR, performer.getId()))));
    }

    private boolean hasTaskChanged(Task oldTask, Task newTask) {
        return !oldTask.getTaskName().equals(newTask.getTaskName()) ||
                !oldTask.getComment().equals(newTask.getComment()) ||
                !oldTask.getStatus().equals(newTask.getStatus()) ||
                !oldTask.getPerformers().equals(newTask.getPerformers()) ||
                !hasSameDeadlines(oldTask.getDeadlines(), newTask.getDeadlines()) ||
                !hasSameDayTimes(oldTask.getDayTimes(), newTask.getDayTimes());
    }

    private boolean hasSameDeadlines(Set<Deadline> oldDeadlines, Set<Deadline> newDeadlines) {
        if (oldDeadlines.size() != newDeadlines.size()) {
            return false;
        }

        for (Deadline oldDeadline : oldDeadlines) {
            if (newDeadlines.stream().noneMatch(newDeadline ->
                                                        Objects.equals(oldDeadline.getExecutionDeadline(), newDeadline.getExecutionDeadline()))) {
                return false;
            }
        }

        return true;
    }

    boolean hasSameDayTimes(Set<DayTime> oldDayTimes, Set<DayTime> newDayTimes) {
        if (oldDayTimes.size() != newDayTimes.size()) {
            return false;
        }

        for (DayTime oldDayTime : oldDayTimes) {
            if (newDayTimes.stream().noneMatch(newDayTime ->
                                                       oldDayTime.getDayOfWeek() == newDayTime.getDayOfWeek() &&
                                                               oldDayTime.getTime().equals(newDayTime.getTime()))) {
                return false;
            }
        }

        return true;
    }

    public Map<Long, Map<Long, Boolean>> getTaskMessageReadStatus(List<TaskDto> taskDtoList) {
        Map<Long, Map<Long, Boolean>> taskMessageReadStatusMap = new HashMap<>();

        for (TaskDto taskDto : taskDtoList) {
            Long taskId = taskDto.getId();
            boolean messageReadOptionUsed = taskToPerformerMessageReadService.isMessageReadOptionForTaskExist(taskDto.getId());
            if (messageReadOptionUsed) {
                Map<Long, Boolean> performerReadStatus = new HashMap<>();
                for (ClientDto performer : taskDto.getPerformers()) {
                    TaskToPerformerMessageRead taskToPerformerMessageRead = taskToPerformerMessageReadService.findByTaskIdAndPerformerId(taskId, performer.getId());
                    if (taskToPerformerMessageRead != null) {
                        performerReadStatus.put(performer.getId(), taskToPerformerMessageRead.isMessageRead());
                    }
                }
                taskMessageReadStatusMap.put(taskId, performerReadStatus);
            }
        }
        return taskMessageReadStatusMap;
    }
}