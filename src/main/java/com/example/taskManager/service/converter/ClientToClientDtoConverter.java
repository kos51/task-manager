    package com.example.taskManager.service.converter;

    import com.example.taskManager.entity.Client;
    import com.example.taskManager.service.dto.ClientDto;
    import org.springframework.core.convert.converter.Converter;
    import org.springframework.stereotype.Service;

    @Service
    public class ClientToClientDtoConverter implements Converter<Client, ClientDto> {

        @Override
        public ClientDto convert(Client source) {
            ClientDto clientDto = new ClientDto().setId(source.getId())
                                                 .setSurname(source.getSurname())
                                                 .setName(source.getName())
                                                 .setPatronymic(source.getPatronymic())
                                                 .setEmail(source.getEmail())
                                                 .setRole(source.getRole().name());
            if (source.getTelegramChatId() == null) {
                clientDto.setTelegramChatId(null);
            } else {
                clientDto.setTelegramChatId(source.getTelegramChatId());
            }
            return clientDto;
        }
    }
