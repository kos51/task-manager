package com.example.taskManager.service.converter;

import com.example.taskManager.security.CustomUserDetails;
import com.example.taskManager.service.dto.ClientDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsToClientDtoConverter implements Converter<CustomUserDetails, ClientDto> {

    @Override
    public ClientDto convert(CustomUserDetails source) {
        ClientDto clientDto = new ClientDto().setId(source.getId())
                                             .setSurname(source.getSurname())
                                             .setName(source.getName())
                                             .setPatronymic(source.getPatronymic())
                                             .setEmail(source.getEmail())
                                             .setRole(source.getAuthority().getAuthority());
        if (source.getTelegramChatId() == null) {
            clientDto.setTelegramChatId(null);
        } else {
            clientDto.setTelegramChatId(source.getTelegramChatId());
        }
        return clientDto;
    }
}
