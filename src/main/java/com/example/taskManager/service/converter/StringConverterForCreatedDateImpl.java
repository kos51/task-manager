package com.example.taskManager.service.converter;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class StringConverterForCreatedDateImpl implements StringConverter {

    @Override
    public String convert(String source) {
        DateTimeFormatter originalFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
        DateTimeFormatter targetFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

        return LocalDateTime.parse(source, originalFormatter).format(targetFormatter);
    }
}
