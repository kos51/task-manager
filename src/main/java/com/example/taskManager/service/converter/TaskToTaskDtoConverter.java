package com.example.taskManager.service.converter;

import com.example.taskManager.entity.Client;
import com.example.taskManager.entity.Task;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Comparator;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service
@RequiredArgsConstructor
public class TaskToTaskDtoConverter implements Converter<Task, TaskDto> {
    private final Converter<Client, ClientDto> clientClientDtoConverter;
    private final StringConverterForCreatedDateImpl stringConverterForCreatedDate;
    private final StringConverterForDeadlineImpl stringConverterForDeadline;
    private final DayTimeToStringConverter dayTimeToStringConverter;

    @Override
    public TaskDto convert(@NotNull Task source) {
        return new TaskDto().setId(source.getId())
                            .setTaskName(source.getTaskName())
                            .setOwnerSurname(source.getOwner().getSurname())
                            .setOwnerName(source.getOwner().getName())
                            .setOwnerPatronymic(source.getOwner().getPatronymic())
                            .setNotificationType(source.getNotificationType())
                            .setCreatedDateTime(
                                    stringConverterForCreatedDate.convert(
                                            (source.getCreatedDateTime() != null) ? source.getCreatedDateTime().toString() : ""
                                    )
                            )
                            .setComment(source.getComment() != null ? source.getComment() : "")
                            .setPerformers((source.getPerformers() != null) ? source.getPerformers()
                                                                                    .stream()
                                                                                    .sorted(Comparator.comparing(Client::getSurname))
                                                                                    .map(clientClientDtoConverter::convert)
                                                                                    .collect(toSet()) : Collections.emptySet())
                            .setStatus((source.getStatus() != null) ? source.getStatus().getMessage() : "")
                            .setDeadlineTask(source.getDeadlines() != null && !source.getDeadlines().isEmpty())
                            .setDeadlines(source.getDeadlines() != null && !source.getDeadlines().isEmpty() ?
                                                  source.getDeadlines().stream()
                                                        .map(deadline -> stringConverterForDeadline.convert(
                                                                deadline.getExecutionDeadline() != null ? deadline.getExecutionDeadline().toString() : ""
                                                        ))
                                                        .collect(toList()) :
                                                  null)
                            .setDayTimes(source.getDayTimes() != null && !source.getDayTimes().isEmpty() ?
                                                 source.getDayTimes().stream()
                                                       .map(dayTimeToStringConverter::convert)
                                                       .collect(toList()) :
                                                 null);
    }
}
