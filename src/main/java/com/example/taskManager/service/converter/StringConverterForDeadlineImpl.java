package com.example.taskManager.service.converter;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class StringConverterForDeadlineImpl implements StringConverter {

    @Override
    public String convert(String source) {
        DateTimeFormatter originalFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
        DateTimeFormatter targetFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy" + " до " + "HH:mm");

        return LocalDateTime.parse(source, originalFormatter).format(targetFormatter);
    }
}
