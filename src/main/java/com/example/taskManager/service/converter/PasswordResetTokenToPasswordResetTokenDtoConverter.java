package com.example.taskManager.service.converter;

import com.example.taskManager.entity.PasswordResetToken;
import com.example.taskManager.service.dto.PasswordResetTokenDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class PasswordResetTokenToPasswordResetTokenDtoConverter implements Converter<PasswordResetToken, PasswordResetTokenDto> {

    @Override
    public PasswordResetTokenDto convert(PasswordResetToken source) {
        return new PasswordResetTokenDto().setId(source.getId())
                                          .setToken(source.getToken())
                                          .setClientId(source.getClient().getId())
                                          .setExpireDate(source.getExpireDate());
    }
}
