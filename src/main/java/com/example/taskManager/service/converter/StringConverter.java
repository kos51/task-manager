package com.example.taskManager.service.converter;

public interface StringConverter {

    public String convert(String source);
}
