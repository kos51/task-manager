package com.example.taskManager.service.converter;

import com.example.taskManager.entity.DayTime;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class DayTimeToStringConverter implements Converter<DayTime, String> {

    @Override
    public String convert(DayTime source) {
        return source.getDayOfWeek().getMessage() + " до " + source.getTime();
    }
}
