package com.example.taskManager.service.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
@RequiredArgsConstructor
@Service
public class TaskDto {
    private Long id;
    private String taskName;
    private String ownerSurname;
    private String ownerName;
    private String ownerPatronymic;
    private String comment;
    private String status;
    private String createdDateTime;
    private Set<ClientDto> performers;
    private boolean isDeadlineTask;
    private List<String> deadlines;
    private List<String> dayTimes;
    private String notificationType;

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<b>").append("№ ").append(id).append("</b>\n");
        sb.append("<b>").append("задача: ").append("</b>").append(taskName).append("\n");
        sb.append("<b>").append("кто создал: ").append("</b>").append("\n").append(ownerSurname).append(" ").append(ownerName).append(" ").append(ownerPatronymic).append("\n");
        sb.append("<b>").append("что сделать: ").append("</b>").append(comment).append("\n");
        sb.append("<b>").append("дата создания: ").append("</b>").append(createdDateTime).append("\n");
        sb.append("<b>").append("исполнители: ").append("</b>").append("\n");
        performers.stream()
                  .map(performer -> performer.getSurname() + " " + performer.getName().charAt(0) + "." + performer.getPatronymic().charAt(0) + ".")
                  .forEach(performer -> sb.append(performer).append("\n"));
        if (isDeadlineTask) {
            sb.append("<b>").append("сроки исполнения: ").append("</b>").append("\n").append(String.join("\n", deadlines)).append("\n");
        } else {
            sb.append("<b>").append("сроки исполнения: ").append("</b>").append("\n").append(String.join("\n", dayTimes)).append("\n");
        }
        return sb.toString();
    }
}
