package com.example.taskManager.service.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@RequiredArgsConstructor
@Service
public class PasswordResetTokenDto {
    private Long id;
    private String token;
    private Long clientId;
    private LocalDateTime expireDate;
}
