package com.example.taskManager.service.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.stereotype.Service;

@Data
@Accessors(chain = true)
@RequiredArgsConstructor
@Service
public class ClientDto {
    private Long id;
    private String surname;
    private String name;
    private String patronymic;
    private String email;
    private Long telegramChatId;
    private String role;
}