package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.security.ClientRole;
import com.example.taskManager.security.CustomUserDetails;
import com.example.taskManager.service.dto.ClientDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
@RequiredArgsConstructor
public class ClientService implements AuthenticationService {
    private final ClientRepository clientRepository;
    private final Converter<Client, ClientDto> clientToClientDtoConverter;
    private final PasswordEncoder passwordEncoder;

    public ClientDto register(String surname, String name, String patronymic, String email, String password, String role) {
        try {
            Client client = new Client().setSurname(surname)
                                        .setName(name)
                                        .setPatronymic(patronymic)
                                        .setEmail(email)
                                        .setPassword(passwordEncoder.encode(password))
                                        .setTelegramChatId(null)
                                        .setRole(ClientRole.valueOf(role));
            return clientToClientDtoConverter.convert(clientRepository.save(client));
        } catch (DataIntegrityViolationException e) {
            throw new ServiceException("Пользователь с email: " + email + " уже существует!");
        }
    }

    public List<ClientDto> findAll() {
        return clientRepository.findAll()
                               .stream()
                               .map(clientToClientDtoConverter::convert)
                               .collect(toList());
    }

    public void updateTelegramChatId(Long clientId, Long telegramChatId) throws ServiceException {
        try {
            if (telegramChatId == null) {
                throw new IllegalArgumentException("Telegram Chat ID не может быть равен null");
            }

            Client client = clientRepository.findById(clientId).orElseThrow(() -> new NotFoundException("Пользователь с ID: " + clientId + " не найден."));
            if (client.getTelegramChatId() == null) {
                client.setTelegramChatId(telegramChatId);
                clientRepository.save(client);
            } else {
                throw new ServiceException("Telegram Chat ID уже зарегистрирован.");
            }
        } catch (IllegalArgumentException | NotFoundException e) {
            log.error("Ошибка: ", e);
        }
    }

    public ClientDto getClientByTelegramChatId(Long chatId) {
        Client client = clientRepository.getClientByTelegramChatId(chatId);
        if (client == null) {
            return null;
        }
        return clientToClientDtoConverter.convert(client);
    }

    public boolean isTelegramChatIdRegisteredByClientId(Long clientId) {
        return clientRepository.existsChatIdByClientId(clientId);
    }

    public ClientDto currentClientDto() {
        try {
            CustomUserDetails client = (CustomUserDetails) SecurityContextHolder.getContext()
                                                                                .getAuthentication()
                                                                                .getPrincipal();
            return clientToClientDtoConverter.convert(clientRepository.getOne(client.getId()));
        } catch (EntityNotFoundException e) {
            throw new NotFoundException("Доступ запрещен!");
        }
    }
}