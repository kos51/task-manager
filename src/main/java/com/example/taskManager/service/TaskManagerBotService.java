package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static java.lang.Long.valueOf;

@Service
@Slf4j
public class TaskManagerBotService extends TelegramLongPollingBot {
    private static final String COMMAND_ACTIVE_TASKS = "активные задачи";
    private static final String COMMAND_CLOSED_TASKS = "завершенные задачи";
    private static final String COMMAND_MY_CREATED_AND_ACTIVE_TASKS = "поставленные мной задачи";
    private static final String ERROR_MESSAGE_SENDING = "Произошла ошибка при отправке сообщения: ";
    private static final String INVALID_COMMAND = "Введена неверная комманда!";
    private final ClientService clientService;
    private final TaskToPerformerMessageReadService taskToPerformerMessageReadService;

    @Lazy
    @Autowired
    private TaskService taskService;

    @Value("${telegram.bot.username}")
    private String botUsername;

    @Value("${telegram.bot.token}")
    private String botToken;

    public TaskManagerBotService(ClientService clientService, TaskToPerformerMessageReadService taskToPerformerMessageReadService) {
        this.clientService = clientService;
        this.taskToPerformerMessageReadService = taskToPerformerMessageReadService;
    }

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasCallbackQuery()) {
                CallbackQuery callbackQuery = update.getCallbackQuery();
                String callbackData = callbackQuery.getData();
                if (callbackData.startsWith("messageReadTrue:")) {
                    handleCallbackQuery(callbackQuery);
                }
            }
            if (update.hasMessage() && update.getMessage().hasText()) {
                String messageText = update.getMessage().getText();
                Long chatId = update.getMessage().getChatId();

                ClientDto clientDto = clientService.getClientByTelegramChatId(chatId);
                if (clientDto != null) {
                    handleOtherCommands(messageText, chatId);
                } else if (messageText.startsWith("/start")) {
                    handleStartCommand(messageText, chatId);
                } else {
                    sendTextMessage(chatId, "Ваш Telegram ID не зарегистрирован, пожалуйста, зарегистрируйтесь для продолжения");
                }
            }
        } catch (Exception e) {
            log.error("Произошла ошибка при обработке обновления", e);
        }
    }

    private void handleCallbackQuery(CallbackQuery callbackQuery) {
        String callbackData = callbackQuery.getData();
        if (callbackData.startsWith("messageReadTrue:")) {
            String[] parts = callbackData.split(":");
            Long taskId = Long.parseLong(parts[1]);
            Long performerId = Long.parseLong(parts[2]);

            taskToPerformerMessageReadService.markMessageAsRead(taskId, performerId);
            sendTextMessage(callbackQuery.getMessage().getChatId(), "прочитано ✅: задача №" + taskId);
            deleteInlineKeyboardButton(callbackQuery);
        }
    }

    private void deleteInlineKeyboardButton(CallbackQuery callbackQuery) {
        EditMessageReplyMarkup editMarkup = new EditMessageReplyMarkup();
        editMarkup.setChatId(callbackQuery.getMessage().getChatId().toString());
        editMarkup.setMessageId(callbackQuery.getMessage().getMessageId());
        editMarkup.setReplyMarkup(null);

        try {
            execute(editMarkup);
        } catch (TelegramApiException e) {
            log.error("Ошибка при удалении кнопки:", e);
        }
    }

    private void handleStartCommand(String messageText, Long chatId) {
        if ("/start".equals(messageText)) {
            String response = "Для начала работы, пожалуйста, зарегистрируйтесь в системе!";
            sendTextMessageWithKeyboard(chatId, response, createMainMenuKeyboard());
        } else if (messageText.startsWith("/start ")) {
            String clientId = messageText.substring("/start ".length());
            processRegistration(clientId, chatId);
        } else {
            sendTextMessageWithKeyboard(chatId, INVALID_COMMAND, createMainMenuKeyboard());
        }
    }

    private void processRegistration(String clientId, Long chatId) {
        try {
            if (!clientService.isTelegramChatIdRegisteredByClientId(valueOf(clientId))) {
                clientService.updateTelegramChatId(valueOf(clientId), chatId);
                sendTextMessageWithKeyboard(chatId, "Регистрация успешно выполнена. Теперь вы можете управлять своими задачами.\n\n", createMainMenuKeyboard());
            }
        } catch (ServiceException e) {
            sendTextMessage(chatId, e.getMessage());
        }
    }

    private void handleOtherCommands(String messageText, Long chatId) {
        if (messageText.equals(COMMAND_ACTIVE_TASKS)) {
            sendActiveTasks(chatId);
        } else if (messageText.equals(COMMAND_CLOSED_TASKS)) {
            sendClosedTasks(chatId);
        } else if (messageText.equals(COMMAND_MY_CREATED_AND_ACTIVE_TASKS)) {
            sendMyCreatedTasks(chatId);
        } else if (messageText.contains("start")) {
            sendTextMessageWithKeyboard(chatId, "Рады видеть вас!\n", createMainMenuKeyboard());
        } else {
            sendTextMessageWithKeyboard(chatId, INVALID_COMMAND, createMainMenuKeyboard());
        }
    }

    private void sendActiveTasks(Long chatId) {
        List<TaskDto> activeTasks = taskService.findActiveTasksByTelegramChatId(chatId);
        if (activeTasks.isEmpty()) {
            sendTextMessage(chatId, "У вас нет задач в работе");
        } else {
            sendTextMessage(chatId, "Активные задачи:\n\n");
            for (TaskDto task : activeTasks) {
                sendTextMessage(chatId, "\uD83D\uDCCC" + task.toString() + "\n");
            }
            createMainMenuKeyboard();
        }
    }

    private void sendClosedTasks(Long chatId) {
        List<TaskDto> closedTasks = taskService.findClosedTasksByTelegramChatId(chatId);
        if (closedTasks.isEmpty()) {
            sendTextMessage(chatId, "У вас нет завершенных задач");
        } else {
            sendTextMessage(chatId, "Завершенные задачи:\n\n");
            for (TaskDto task : closedTasks) {
                sendTextMessage(chatId, "\uD83D\uDCCC " + task.toString() + "\n");
            }
            createMainMenuKeyboard();
        }
    }

    private void sendMyCreatedTasks(Long chatId) {
        List<TaskDto> myCreatedAndActiveTasks = taskService.findMyCreatedAndActiveTasksByTelegramChatId(chatId);
        if (myCreatedAndActiveTasks.isEmpty()) {
            sendTextMessage(chatId, "Поставленных задач нет");
        } else {
            sendTextMessage(chatId, "Поставленные вами задачи:\n\n");
            for (TaskDto task : myCreatedAndActiveTasks) {
                sendTextMessage(chatId, "\uD83D\uDCCC " + task.toString() + "\n");
            }
            createMainMenuKeyboard();
        }
    }

    public void sendTextMessage(Long chatId, String response) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(response);
        message.setParseMode("HTML");

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_MESSAGE_SENDING, e);
        }
    }

    public void sendTelegramNotifications(Set<Client> performers, TaskDto taskDto, String taskTitle, boolean messageReadStatus) {
        StringBuilder messageBuilder = new StringBuilder("<b>").append(taskTitle).append("</b>\n").append(taskDto.toString());
        if (messageReadStatus) {
            for (Client performer : performers) {
                InlineKeyboardMarkup inlineKeyboardMarkup = getInlineKeyboardMarkup(taskDto, performer);
                sendTextMessageWithInlineKeyboard(performer.getTelegramChatId(), messageBuilder.toString(), inlineKeyboardMarkup);
            }
        } else {
            for (Client performer : performers) {
                sendTextMessage(performer.getTelegramChatId(), messageBuilder.toString());
            }
        }
    }

    private InlineKeyboardMarkup getInlineKeyboardMarkup(TaskDto taskDto, Client performer) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> keyboard = new ArrayList<>();
        List<InlineKeyboardButton> row = new ArrayList<>();

        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText("принял");
        button.setCallbackData("messageReadTrue:" + taskDto.getId() + ":" + performer.getId());
        row.add(button);

        keyboard.add(row);
        inlineKeyboardMarkup.setKeyboard(keyboard);
        return inlineKeyboardMarkup;
    }

    private ReplyKeyboardMarkup createMainMenuKeyboard() {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboard = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        KeyboardRow row1 = new KeyboardRow();
        row.add(new KeyboardButton(COMMAND_ACTIVE_TASKS));
        row.add(new KeyboardButton(COMMAND_CLOSED_TASKS));
        row1.add(new KeyboardButton(COMMAND_MY_CREATED_AND_ACTIVE_TASKS));
        keyboard.add(row);
        keyboard.add(row1);
        replyKeyboardMarkup.setKeyboard(keyboard);
        return replyKeyboardMarkup;
    }

    private void sendTextMessageWithKeyboard(Long chatId, String response, ReplyKeyboardMarkup replyKeyboardMarkup) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(response);
        message.setReplyMarkup(replyKeyboardMarkup);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_MESSAGE_SENDING, e);
        }
    }

    private void sendTextMessageWithInlineKeyboard(Long chatId, String response, InlineKeyboardMarkup inlineKeyboardMarkup) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(response);
        message.setParseMode("HTML");
        message.setReplyMarkup(inlineKeyboardMarkup);

        try {
            execute(message);
        } catch (TelegramApiException e) {
            log.error(ERROR_MESSAGE_SENDING, e);
        }
    }
}