package com.example.taskManager.service;

import com.example.taskManager.entity.TaskToPerformerMessageRead;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.repository.TaskToPerformerMessageReadRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class TaskToPerformerMessageReadServiceImpl implements TaskToPerformerMessageReadService {
    private static final String UPDATE_MESSAGE_STATUS_ERROR = "Ошибка при обновлении статуса сообщения для задачи с ID: ";
    private final TaskToPerformerMessageReadRepository taskToPerformerMessageReadRepository;

    @Override
    public void createTaskToPerformerMessageReadRecords(Long taskId, List<Long> performerIds) {
        for (Long performerId : performerIds) {
            TaskToPerformerMessageRead messageRead = new TaskToPerformerMessageRead().setTaskId(taskId)
                                                                                     .setPerformerId(performerId)
                                                                                     .setMessageRead(false);
            taskToPerformerMessageReadRepository.save(messageRead);
        }
    }

    @Override
    public boolean existsByTaskIdAndPerformerId(Long taskId, Long performerId) {
        return taskToPerformerMessageReadRepository.existsByTaskIdAndPerformerId(taskId, performerId);
    }

    @Override
    @Transactional
    public void markMessageAsRead(Long taskId, Long performerId) {
        markMessage(taskId, performerId, true);
    }

    @Transactional
    public void markMessageAsNotRead(Long taskId, Long performerId) {
        markMessage(taskId, performerId, false);
    }

    @Transactional
    @Override
    public void updateMessageReadStatus(Long taskId, List<Long> performerIds, boolean isMessageRead) {
        try {
            for (Long performerId : performerIds) {
                taskToPerformerMessageReadRepository.updateMessageReadStatus(taskId, performerId, isMessageRead);
            }
        } catch (Exception e) {
            log.error(UPDATE_MESSAGE_STATUS_ERROR + taskId, e);
            throw new ServiceException(e);
        }
    }

    public boolean isMessageReadOptionForTaskExist(Long taskId) {
        return !taskToPerformerMessageReadRepository.findByTaskId(taskId).isEmpty();
    }

    @Override
    public TaskToPerformerMessageRead findByTaskIdAndPerformerId(Long taskId, Long performerId) {
        return taskToPerformerMessageReadRepository.findByTaskIdAndPerformerId(taskId, performerId);
    }

    private void markMessage(Long taskId, Long performerId, boolean isRead) {
        TaskToPerformerMessageRead messageRead = taskToPerformerMessageReadRepository.findByTaskIdAndPerformerId(taskId, performerId);
        if (messageRead == null) {
            messageRead = new TaskToPerformerMessageRead().setTaskId(taskId)
                                                          .setPerformerId(performerId);
        }
        messageRead.setMessageRead(isRead);
        taskToPerformerMessageReadRepository.save(messageRead);
    }

}

