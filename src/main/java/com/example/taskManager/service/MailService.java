package com.example.taskManager.service;

public interface MailService {
    void sendHtmlEmail(String to, String subject, String htmlContent);
}
