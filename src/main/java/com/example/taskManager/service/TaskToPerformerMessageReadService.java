package com.example.taskManager.service;

import com.example.taskManager.entity.TaskToPerformerMessageRead;

import java.util.List;

public interface TaskToPerformerMessageReadService {
    void createTaskToPerformerMessageReadRecords(Long taskId, List<Long> performerIds);
    void updateMessageReadStatus(Long taskId, List<Long> performerIds, boolean isMessageRead);
    boolean existsByTaskIdAndPerformerId(Long taskId, Long performerId);
    void markMessageAsRead(Long taskId, Long performerId);
    boolean isMessageReadOptionForTaskExist(Long taskId);
    TaskToPerformerMessageRead findByTaskIdAndPerformerId(Long taskId, Long performerId);
}
