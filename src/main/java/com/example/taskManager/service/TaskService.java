package com.example.taskManager.service;

import com.example.taskManager.entity.Task;
import com.example.taskManager.enums.DayOfWeek;
import com.example.taskManager.service.dto.TaskDto;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public interface TaskService {

    TaskDto findByIdAndOwnerId(Long taskId, Long ownerId);

    List<TaskDto> findActiveTasksByTelegramChatId(Long chatId);

    List<TaskDto> findClosedTasksByTelegramChatId(Long chatId);

    List<TaskDto> findMyCreatedAndActiveTasksByTelegramChatId(Long chatId);

    List<Task> getAllTasksByDeadline(LocalDateTime deadline);

    List<Task> getAllTasksByDayTime(DayOfWeek dayOfWeek, LocalTime currentTime);

    List<Task> getAllTasksByNotificationType(String notificationType);
}
