package com.example.taskManager.web.controller;

import com.example.taskManager.TaskManagerApplication;
import com.example.taskManager.security.CustomAuthenticationFailureHandler;
import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.converter.StringConverterForCreatedDateImpl;
import com.example.taskManager.service.converter.StringConverterForDeadlineImpl;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.web.form.RegisterForm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LoginAndRegController.class)
@Import({TaskManagerApplication.class, MockSecurityConfiguration.class})
@ExtendWith(MockitoExtension.class)
class LoginAndRegControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @MockBean
    ClientService clientService;

    @MockBean
    StringConverterForCreatedDateImpl stringConverterForCreatedDate;

    @MockBean
    StringConverterForDeadlineImpl stringConverterForDeadline;

    @Test
    public void getLogin_withErrors() throws Exception {
        mockMvc.perform(get("/login").param("error", "true"))
               .andExpect(status().isOk())
               .andExpect(model().attributeExists("infoMessage"))
               .andExpect(model().attribute("infoMessage", "Неверный логин или пароль!"))
               .andExpect(view().name("login-form"));
    }

    @Test
    public void getLogin_withExpired() throws Exception {
        mockMvc.perform(get("/login").param("expired", "true"))
               .andExpect(status().isOk())
               .andExpect(model().attributeExists("infoMessage"))
               .andExpect(model().attribute("infoMessage", "Превышен лимит одновременных сессий. Попробуйте снова позже"))
               .andExpect(view().name("login-form"));
    }

    @Test
    public void getRegister_ok() throws Exception {
        mockMvc.perform(get("/register"))
               .andExpect(status().isOk())
               .andExpect(model().attributeHasNoErrors())
               .andExpect(model().attributeExists("form"))
               .andExpect(view().name("register"));
    }

    @Test
    public void postRegister_ok() throws Exception { //todo - не работает при сборке!
        Long clientId = 123L;
        ClientDto clientDto = new ClientDto().setId(clientId);

        RegisterForm registerForm = new RegisterForm().setSurname("Surname")
                                                      .setName("Name")
                                                      .setPatronymic("Patronymic")
                                                      .setEmail("example@mail.ru")
                                                      .setPassword("password")
                                                      .setRole("SUPERVISOR");

        when(clientService.register(anyString(), anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(clientDto);

        mockMvc.perform(post("/register").flashAttr("form", registerForm))
               .andExpect(status().is3xxRedirection());
    }
}