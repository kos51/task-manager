package com.example.taskManager.web.controller;

import com.example.taskManager.TaskManagerApplication;
import com.example.taskManager.entity.Client;
import com.example.taskManager.entity.Task;
import com.example.taskManager.enums.StatusCode;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.security.ClientRole;
import com.example.taskManager.security.CustomAuthenticationFailureHandler;
import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.TaskServiceImpl;
import com.example.taskManager.service.converter.StringConverterForCreatedDateImpl;
import com.example.taskManager.service.converter.StringConverterForDeadlineImpl;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import com.example.taskManager.web.form.TaskCreateForm;
import com.example.taskManager.web.form.TaskUpdateForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(TaskController.class)
@Import({TaskManagerApplication.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
class TaskControllerTest {
    private final String taskName = "myTaskName";
    private final String comment = "myComment";
    private final String notificationType = "HOUR";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ClientService clientService;

    @MockBean
    TaskServiceImpl taskService;

    @MockBean
    CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @MockBean
    StringConverterForCreatedDateImpl stringConverterForCreatedDate;

    @MockBean
    StringConverterForDeadlineImpl stringConverterForDeadline;
    private ClientDto clientDto;
    private Set<Client> performers;

    @BeforeEach
    void setUp() {
        Client client = new Client().setId(1L)
                                    .setSurname("mySurname")
                                    .setName("myName")
                                    .setPatronymic("myPatronymic")
                                    .setEmail("email@mail.ru")
                                    .setPassword("encodedPassword")
                                    .setTelegramChatId(123456789L)
                                    .setRole(ClientRole.valueOf("SUPERVISOR"));

        clientDto = new ClientDto().setId(client.getId())
                                   .setSurname(client.getSurname())
                                   .setName(client.getName())
                                   .setPatronymic(client.getPatronymic())
                                   .setEmail(client.getEmail())
                                   .setTelegramChatId(client.getTelegramChatId())
                                   .setRole(client.getRole().toString());

        performers = singleton(client);
    }

    @Test
    @WithUserDetails(value = "kos51@bk.ru", userDetailsServiceBeanName = "userDetailsService")
    void createTaskWithDayTimes_ok() throws Exception {
        when(clientService.currentClientDto()).thenReturn(clientDto);

        TaskCreateForm taskCreateForm = new TaskCreateForm().setTaskName(taskName)
                                                            .setComment(comment)
                                                            .setPerformers(performers)
                                                            .setWeeklyDay("Monday")
                                                            .setWeeklyTime("12:00")
                                                            .setNotificationType(notificationType);

        mockMvc.perform(post("/personalArea/task/create").param("deadlineType", "WEEKDAYS").flashAttr("form", taskCreateForm))
               .andExpect(status().is3xxRedirection())
               .andExpect(flash().attribute("infoMessage", "Задача успешно создана!"))
               .andExpect(redirectedUrl("/personalArea"));

        verify(clientService, times(1)).currentClientDto();
    }

    @Test
    @WithUserDetails(value = "kos51@bk.ru", userDetailsServiceBeanName = "userDetailsService")
    void createTaskWithDeadlines_ok() throws Exception {
        when(clientService.currentClientDto()).thenReturn(clientDto);

        TaskCreateForm taskCreateForm = new TaskCreateForm().setTaskName(taskName)
                                                            .setComment(comment)
                                                            .setPerformers(performers)
                                                            .setDeadlines(singletonList("2024-01-01'T'14:00"))
                                                            .setNotificationType(notificationType);

        mockMvc.perform(post("/personalArea/task/create").param("deadlineType", "DATES").flashAttr("form", taskCreateForm))
               .andExpect(status().is3xxRedirection())
               .andExpect(flash().attribute("infoMessage", "Задача успешно создана!"))
               .andExpect(redirectedUrl("/personalArea"));

        verify(clientService, times(1)).currentClientDto();
    }

    @Test
    @WithUserDetails(value = "kos51@bk.ru", userDetailsServiceBeanName = "userDetailsService")
    void updateTaskWithDeadlines_ok() throws Exception {
        Set<Client> updatePerformers = singleton(new Client().setId(2L));

        when(clientService.currentClientDto()).thenReturn(clientDto);

        Task task = new Task().setId(1L)
                              .setTaskName(taskName)
                              .setStatus(StatusCode.ACTIVE);

        TaskDto taskDto = new TaskDto().setId(task.getId())
                                       .setTaskName(task.getTaskName())
                                       .setStatus(task.getStatus().name());

        when(taskService.findByIdAndOwnerId(1L, 1L)).thenReturn(taskDto);

        TaskUpdateForm taskUpdateForm = new TaskUpdateForm().setTaskId(1L)
                                                            .setTaskName(taskName)
                                                            .setComment(comment)
                                                            .setStatus("на исполнении")
                                                            .setUpdatePerformers(updatePerformers)
                                                            .setUpdateDeadlines(singletonList("2024-01-01'T'14:00"))
                                                            .setNotificationType(notificationType);

        mockMvc.perform(post("/personalArea/task/update").param("deadlineType", "DATES").flashAttr("form", taskUpdateForm))
               .andExpect(status().is3xxRedirection())
               .andExpect(flash().attribute("infoMessage", "Задача успешно обновлена!"))
               .andExpect(redirectedUrl("/personalArea"));

        verify(clientService, times(1)).currentClientDto();
    }

    @Test
    @WithUserDetails(value = "kos51@bk.ru", userDetailsServiceBeanName = "userDetailsService")
    void deleteTask_ok() throws Exception {
        when(clientService.currentClientDto()).thenReturn(clientDto);

        mockMvc.perform(post("/personalArea/task/delete/{taskId}", 1L))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/personalArea"))
               .andExpect(flash().attribute("infoMessage", "Задача успешно удалена!"));

        verify(taskService).deleteTask(1L, 1L);
    }

    @Test
    @WithUserDetails(value = "kos51@bk.ru", userDetailsServiceBeanName = "userDetailsService")
    void deleteTask_UnsuccessfulDeletion() throws Exception {
        Long taskId = 1L;
        Long ownerId = 1L;
        String errorMessage = "Ошибка при удалении задачи";

        when(clientService.currentClientDto()).thenReturn(clientDto);

        doThrow(new ServiceException(errorMessage)).when(taskService).deleteTask(taskId, ownerId);

        mockMvc.perform(post("/personalArea/task/delete/{taskId}", taskId))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/personalArea"))
               .andExpect(flash().attribute("infoMessage", errorMessage));
    }
}