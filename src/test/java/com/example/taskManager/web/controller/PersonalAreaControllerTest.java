package com.example.taskManager.web.controller;

import com.example.taskManager.TaskManagerApplication;
import com.example.taskManager.security.ClientRole;
import com.example.taskManager.security.CustomAuthenticationFailureHandler;
import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.TaskServiceImpl;
import com.example.taskManager.service.converter.StringConverterForCreatedDateImpl;
import com.example.taskManager.service.converter.StringConverterForDeadlineImpl;
import com.example.taskManager.service.dto.ClientDto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PersonalAreaController.class)
@Import({TaskManagerApplication.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
class PersonalAreaControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TaskServiceImpl taskService;

    @MockBean
    ClientService clientService;

    @MockBean
    CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @MockBean
    StringConverterForCreatedDateImpl stringConverterForCreatedDate;

    @MockBean
    StringConverterForDeadlineImpl stringConverterForDeadline;

    @Test
    @WithUserDetails(value = "kos51@bk.ru", userDetailsServiceBeanName = "userDetailsService")
    void getPersonalArea_ok() throws Exception {
        ClientDto clientDto = new ClientDto().setId(1L)
                                             .setSurname("Сухопаров")
                                             .setName("Константин")
                                             .setPatronymic("Алексеевич")
                                             .setEmail("kos51@bk.ru")
                                             .setTelegramChatId(12345678L)
                                             .setRole(ClientRole.SUPERVISOR.name());

        when(clientService.currentClientDto()).thenReturn(clientDto);

        mockMvc.perform(get("/personalArea"))
               .andExpect(status().isOk())
               .andExpect(model().attributeHasNoErrors())
               .andExpect(model().attribute("name", clientDto.getName()))
               .andExpect(model().attributeExists("tasks", "users"))
               .andExpect(view().name("personalArea"));
    }
}