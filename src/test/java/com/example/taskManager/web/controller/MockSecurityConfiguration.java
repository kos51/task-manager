package com.example.taskManager.web.controller;

import com.example.taskManager.security.ClientRole;
import com.example.taskManager.security.CustomGrantedAuthority;
import com.example.taskManager.security.CustomUserDetails;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;

@Configuration
public class MockSecurityConfiguration {

    @Bean
    public UserDetailsService userDetailsService() {
        return username -> new CustomUserDetails(1L,
                                                 "Сухопаров",
                                                 "Константин",
                                                 "Алексеевич",
                                                 "kos51@bk.ru",
                                                 "koskokos",
                                                 123456789L,
                                                 new CustomGrantedAuthority(ClientRole.PERFORMER));
    }
}
