package com.example.taskManager.web.controller;

import com.example.taskManager.TaskManagerApplication;
import com.example.taskManager.security.CustomAuthenticationFailureHandler;
import com.example.taskManager.service.ClientService;
import com.example.taskManager.service.converter.StringConverterForCreatedDateImpl;
import com.example.taskManager.service.converter.StringConverterForDeadlineImpl;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(StartController.class)
@Import({TaskManagerApplication.class, MockSecurityConfiguration.class})
@RunWith(SpringRunner.class)
class StartControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @MockBean
    ClientService clientService;

    @MockBean
    StringConverterForCreatedDateImpl stringConverterForCreatedDate;

    @MockBean
    StringConverterForDeadlineImpl stringConverterForDeadline;

    @Test
    void getStart_ok() throws Exception {
        mockMvc.perform(get("/"))
               .andExpect(status().isOk())
               .andExpect(view().name("start"));
    }

    @Test
    void getStart_hasInfoMessage() throws Exception {
        mockMvc.perform(get("/").flashAttr("infoMessage", "Hi!"))
               .andExpect(status().isOk())
               .andExpect(view().name("start"))
               .andExpect(model().attribute("infoMessage", "Hi!"));
    }
}