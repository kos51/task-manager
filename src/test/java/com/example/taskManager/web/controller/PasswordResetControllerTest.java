package com.example.taskManager.web.controller;

import com.example.taskManager.TaskManagerApplication;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.security.CustomAuthenticationFailureHandler;
import com.example.taskManager.service.MailServiceImpl;
import com.example.taskManager.service.PasswordResetTokenServiceImpl;
import com.example.taskManager.service.converter.StringConverterForCreatedDateImpl;
import com.example.taskManager.service.converter.StringConverterForDeadlineImpl;
import com.example.taskManager.service.dto.PasswordResetTokenDto;
import com.example.taskManager.web.form.EmailForm;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PasswordResetController.class)
@Import(TaskManagerApplication.class)
@RunWith(SpringRunner.class)
class PasswordResetControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PasswordResetTokenServiceImpl passwordResetTokenService;

    @MockBean
    MailServiceImpl mailService;

    @MockBean
    CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    @MockBean
    StringConverterForCreatedDateImpl stringConverterForCreatedDate;

    @MockBean
    StringConverterForDeadlineImpl stringConverterForDeadline;

    @Test
    public void showForgotPasswordForm_ok() throws Exception {
        mockMvc.perform(get("/forgot-password"))
               .andExpect(status().isOk())
               .andExpect(model().attributeHasNoErrors())
               .andExpect(model().attribute("emailForm", new EmailForm()))
               .andExpect(view().name("forgot-password-form"));
    }

    @Test
    public void processForgotPassword_ok() throws Exception {
        String email = "kos51@bk.ru";
        EmailForm emailForm = new EmailForm().setEmail(email);

        PasswordResetTokenDto passwordResetTokenDto = new PasswordResetTokenDto().setId(1L)
                                                                                 .setToken("12345")
                                                                                 .setClientId(1L)
                                                                                 .setExpireDate(LocalDateTime.now());

        when(passwordResetTokenService.requestPasswordReset(emailForm.getEmail())).thenReturn(passwordResetTokenDto);


        mockMvc.perform(post("/forgot-password").flashAttr("emailForm", emailForm))
               .andExpect(status().isOk())
               .andExpect(model().attribute("infoMessage", "Ссылка отправлена на ваш email!"))
               .andExpect(view().name("login-form"));

        verify(passwordResetTokenService, times(1)).requestPasswordReset(emailForm.getEmail());
    }

    @Test
    public void processForgotPassword_notFoundException_emailNotFound() throws Exception {
        String email = "kos51@bk.ru";
        EmailForm emailForm = new EmailForm().setEmail(email);
        String exceptionMessage = "Пользователь с email: " + emailForm.getEmail() + " не найден!";

        when(passwordResetTokenService.requestPasswordReset(emailForm.getEmail())).thenThrow(new NotFoundException(exceptionMessage));

        mockMvc.perform(post("/forgot-password").flashAttr("emailForm", emailForm))
               .andExpect(status().isOk())
               .andExpect(model().attribute("infoMessage", exceptionMessage))
               .andExpect(view().name("login-form"));
    }

    @Test
    public void showResetPasswordForm_ok() throws Exception {
        String token = "12345";

        mockMvc.perform(get("/reset-password").param("token", token))
               .andExpect(status().isOk())
               .andExpect(model().attributeHasNoErrors())
               .andExpect(model().attribute("token", token))
               .andExpect(view().name("reset-password-form"));
    }

    @Test
    public void showResetPasswordForm_tokenNotPresent() throws Exception {
        mockMvc.perform(get("/reset-password"))
               .andExpect(status().isBadRequest());
    }

    @Test
    public void resetPassword_ok() throws Exception {
        String token = "12345";
        String password = "examplePassword1";

        doNothing().when(passwordResetTokenService).resetPassword(token, password);

        mockMvc.perform(post("/reset-password").param("token", token).param("password", password))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/login"));
    }

    @Test
    public void resetPassword_serviceException() throws Exception {
        String token = "12345";
        String password = "examplePassword1";
        String exceptionMessage = "Недействительный или истекший токен";

        doThrow(new ServiceException(exceptionMessage)).when(passwordResetTokenService).resetPassword(token, password);

        mockMvc.perform(post("/reset-password").param("token", token).param("password", password))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl("/login"));
    }
}