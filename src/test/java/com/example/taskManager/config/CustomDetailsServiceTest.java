package com.example.taskManager.config;

import com.example.taskManager.entity.Client;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.security.CustomDetailsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import static com.example.taskManager.security.ClientRole.SUPERVISOR;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class CustomDetailsServiceTest {

    @Mock
    private ClientRepository clientRepository;
    private CustomDetailsService customDetailsService;

    @Before
    public void setUp() {
        customDetailsService = new CustomDetailsService(clientRepository);

        Client client = new Client().setId(1L)
                                    .setName("myName")
                                    .setSurname("mySurname")
                                    .setPatronymic("myPatronymic")
                                    .setEmail("myEmail@gmail.com")
                                    .setPassword("myPassword")
                                    .setTelegramChatId(12345678L)
                                    .setRole(SUPERVISOR);

        when(clientRepository.findByEmail("myEmail@gmail.com")).thenReturn(client);
    }

    @Test
    public void test_load_user_details_by_email() {
        String email = "myEmail@gmail.com";

        Client client = clientRepository.findByEmail(email);
        assertThat(client).isNotNull();

        UserDetails userDetails = customDetailsService.loadUserByUsername(email);

        assertThat(userDetails).isNotNull()
                               .extracting("id", "surname", "name", "patronymic", "email", "password", "telegramChatId")
                               .containsExactly(client.getId(), client.getSurname(), client.getName(), client.getPatronymic(),
                                                client.getEmail(), client.getPassword(), client.getTelegramChatId());
        assertThat(userDetails.getAuthorities()).hasSize(1)
                                                .extracting("authority")
                                                .containsExactly(client.getRole().name());
    }

    @Test
    public void test_throw_username_not_found_exception_when_user_not_found_by_email() {
        String email = "nonexistent@example.com";

        assertThat(clientRepository.findByEmail(email)).isNull();
        assertThrows(UsernameNotFoundException.class, () -> customDetailsService.loadUserByUsername(email));
    }
}