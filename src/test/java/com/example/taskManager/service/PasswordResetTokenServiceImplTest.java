package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.entity.PasswordResetToken;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.repository.PasswordResetTokenRepository;
import com.example.taskManager.security.ClientRole;
import com.example.taskManager.service.converter.PasswordResetTokenToPasswordResetTokenDtoConverter;
import com.example.taskManager.service.dto.PasswordResetTokenDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PasswordResetTokenServiceImplTest {

    private final String email = "myEmail@mail.ru";
    private final String token = "83091829-4351-442a-bb69-1fe970cef613";

    @Mock
    ClientRepository clientRepository;

    @Mock
    PasswordResetTokenRepository passwordResetTokenRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    PasswordResetTokenToPasswordResetTokenDtoConverter passwordResetTokenDtoConverter;

    @InjectMocks
    PasswordResetTokenServiceImpl subj;
    private Client client;
    private PasswordResetToken passwordResetToken;

    @BeforeEach
    public void setUp() {
        String encodedPassword = "encodedPassword";
        client = new Client().setId(1L)
                             .setSurname("performerSurname")
                             .setName("performerName")
                             .setPatronymic("performerPatronymic")
                             .setEmail(email)
                             .setPassword(encodedPassword)
                             .setTelegramChatId(23456789L)
                             .setRole(ClientRole.PERFORMER);

        passwordResetToken = new PasswordResetToken().setToken(token)
                                                     .setClient(client)
                                                     .setExpireDate(LocalDateTime.now().plusHours(1));
    }

    @Test
    void requestPasswordReset_ok() {
        when(clientRepository.findByEmail(email)).thenReturn(client);

        PasswordResetTokenDto expectedPasswordResetTokenDto = new PasswordResetTokenDto().setToken(passwordResetToken.getToken())
                                                                                         .setClientId(passwordResetToken.getClient().getId())
                                                                                         .setExpireDate(passwordResetToken.getExpireDate());

        when(passwordResetTokenDtoConverter.convert(any())).thenReturn(expectedPasswordResetTokenDto);

        PasswordResetTokenDto passwordResetTokenDto = subj.requestPasswordReset(email);

        assertNotNull(passwordResetTokenDto);
        assertEquals(passwordResetTokenDto.getClientId(), expectedPasswordResetTokenDto.getClientId());
        assertEquals(passwordResetTokenDto.getExpireDate(), expectedPasswordResetTokenDto.getExpireDate());

        verify(clientRepository, times(1)).findByEmail(email);
        verify(passwordResetTokenDtoConverter, times(1)).convert(any());
    }

    @Test
    void requestPasswordReset_NotFoundException() {
        when(clientRepository.findByEmail(email)).thenReturn(null);

        assertThrows(NotFoundException.class, () -> subj.requestPasswordReset(email));
    }

    @Test
    void resetPassword_ok() {
        String newPassword = "newPassword";
        String newEncodePassword = "newEncodePassword";
        when(passwordResetTokenRepository.findByToken(token)).thenReturn(passwordResetToken);

        Client client = passwordResetToken.getClient();

        when(passwordEncoder.encode(newPassword)).thenReturn(newEncodePassword);

        client.setPassword(newEncodePassword);

        when(clientRepository.save(client)).thenReturn(client);
        doNothing().when(passwordResetTokenRepository).deleteByTokenId(passwordResetToken.getId());

        subj.resetPassword(token, newPassword);

        verify(passwordResetTokenRepository, times(1)).findByToken(token);
        verify(passwordEncoder, times(1)).encode(newPassword);
        verify(clientRepository, times(1)).save(client);
        verify(passwordResetTokenRepository, times(1)).deleteByTokenId(passwordResetToken.getId());
    }

    @Test
    void resetPassword_ServiceException_resetTokenIsNull() {
        lenient().when(passwordResetTokenRepository.findById(1L)).thenReturn(null);

        assertThrows(ServiceException.class, () -> subj.resetPassword(token, "newPassword"));
    }

    @Test
    void resetPassword_ServiceException_resetTokenIsExpired() {
        LocalDateTime expireDate = LocalDateTime.now().minusHours(1);
        PasswordResetToken expiredToken = new PasswordResetToken().setToken(token)
                                                                  .setClient(client)
                                                                  .setExpireDate(expireDate);

        when(passwordResetTokenRepository.findByToken(token)).thenReturn(expiredToken);

        assertThrows(ServiceException.class, () -> subj.resetPassword(token, "newPassword"));

        verify(passwordResetTokenRepository, never()).deleteByTokenId(anyLong());
        verify(clientRepository, never()).save(any(Client.class));
    }
}