package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.entity.Deadline;
import com.example.taskManager.entity.Task;
import com.example.taskManager.enums.DayOfWeek;
import com.example.taskManager.enums.StatusCode;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.repository.TaskRepository;
import com.example.taskManager.security.ClientRole;
import com.example.taskManager.service.converter.TaskToTaskDtoConverter;
import com.example.taskManager.service.dto.ClientDto;
import com.example.taskManager.service.dto.TaskDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.Collections.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {
    private final Long id = 1L;
    private final LocalDateTime taskExecutionDeadline = LocalDateTime.of(2024, 3, 25, 10, 30, 0);
    private final Long telegramChatId = 12345678L;
    private final String taskName = "myTaskName";
    private final StatusCode status = StatusCode.ACTIVE;
    private final String notificationType = "HOUR";

    @Mock
    ClientRepository clientRepository;

    @Mock
    TaskRepository taskRepository;

    @Mock
    TaskToTaskDtoConverter taskToTaskDtoConverter;
    private Set<Client> performers;
    private Client owner;
    private Task task;
    private TaskDto taskDto;
    @InjectMocks
    private TaskServiceImpl subj;

    @BeforeEach
    public void setUp() {
        Client performer = new Client().setId(id)
                                       .setSurname("performerSurname")
                                       .setName("performerName")
                                       .setPatronymic("performerPatronymic")
                                       .setEmail("performerMail@mail.ru")
                                       .setPassword("performerEncodedPassword")
                                       .setTelegramChatId(23456789L)
                                       .setRole(ClientRole.PERFORMER);

        performers = singleton(performer);

        ClientDto performerDto = new ClientDto().setId(performer.getId())
                                                .setSurname(performer.getSurname())
                                                .setName(performer.getName())
                                                .setPatronymic(performer.getPatronymic())
                                                .setEmail(performer.getEmail())
                                                .setTelegramChatId(performer.getTelegramChatId())
                                                .setRole(performer.getRole().name());

        owner = new Client().setId(id)
                            .setSurname("ownerSurname")
                            .setName("ownerName")
                            .setPatronymic("ownerPatronymic")
                            .setEmail("ownerMail@mail.ru")
                            .setPassword("ownerEncodedPassword")
                            .setTelegramChatId(telegramChatId)
                            .setRole(ClientRole.SUPERVISOR);

        String comment = "myComment";
        task = new Task().setId(id)
                         .setTaskName(taskName)
                         .setComment(comment)
                         .setCreatedDateTime(LocalDateTime.now())
                         .setStatus(status)
                         .setOwner(owner)
                         .setPerformers(performers)
                         .setDayTimes(null)
                         .setDeadlines(new HashSet<>())
                         .setNotificationType(notificationType);

        Set<Deadline> deadlines = singleton(new Deadline().setId(id)
                                                          .setExecutionDeadline(taskExecutionDeadline)
                                                          .setTask(task));
        task.setDeadlines(deadlines);

        taskDto = new TaskDto().setId(task.getId())
                               .setTaskName(task.getTaskName())
                               .setOwnerSurname(task.getOwner().getSurname())
                               .setOwnerSurname(task.getOwner().getName())
                               .setOwnerPatronymic(task.getOwner().getPatronymic())
                               .setComment(task.getComment())
                               .setStatus(task.getStatus().getMessage())
                               .setCreatedDateTime(LocalDateTime.parse(task.getCreatedDateTime().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS"))
                                                                .format(DateTimeFormatter.ofPattern("dd.MM.yyyy")))
                               .setPerformers(singleton(performerDto))
                               .setDeadlineTask(true)
                               .setDeadlines(singletonList("06.11.2024" + " до " + "12:00"))
                               .setNotificationType(task.getNotificationType());
    }

    @Test
    public void testSortDeadlines_ok() {
        List<String> deadlines = Arrays.asList("01.01.2024 до 12:00", "01.01.2024 до 10:00", "02.01.2024 до 08:00");

        List<String> sortedDeadlines = subj.sortDeadlines(deadlines);

        List<String> expectedSortedDeadlines = Arrays.asList("01.01.2024 до 10:00", "01.01.2024 до 12:00", "02.01.2024 до 08:00");

        assertEquals(expectedSortedDeadlines, sortedDeadlines);
    }

    @Test
    public void testSortDeadlines_withEmptyList() {
        List<String> deadlines = emptyList();

        List<String> sortedDeadlines = subj.sortDeadlines(deadlines);

        Assertions.assertTrue(sortedDeadlines.isEmpty());
    }

    @Test
    public void isValid_ok() {
        subj.isValid(taskName, task.getPerformers(), task.getOwner().getId());

        assertNotNull(taskName);
        assertNotNull(task.getOwner().getId());
        assertFalse(task.getPerformers().isEmpty());
    }

    @Test
    public void isValid_emptyTaskName() {
        assertThrows(IllegalArgumentException.class, () -> subj.isValid("", task.getPerformers(), task.getOwner().getId()));
    }

    @Test
    public void isValid_emptyPerformers() {
        assertThrows(IllegalArgumentException.class, () -> subj.isValid(taskName, emptySet(), task.getOwner().getId()));
    }

    @Test
    public void isValid_nullOwnerId() {
        assertThrows(IllegalArgumentException.class, () -> subj.isValid(taskName, task.getPerformers(), null));
    }

    @Test
    public void checkPerformersExists_performerNotFound() {
        for (Client performer : performers) {
            lenient().when(clientRepository.findById(performer.getId())).thenThrow(EntityNotFoundException.class);
        }

        assertThrows(EntityNotFoundException.class, () -> subj.checkPerformersExists(performers));
    }

    @Test
    void checkTaskIsExist_ok() {
        assertDoesNotThrow(() -> subj.checkTaskIsExist(task, "errorCode"));
    }

    @Test
    void checkTaskIsExist_taskIsNull() {
        assertThrows(NotFoundException.class, () -> subj.checkTaskIsExist(null, "errorCode"));
    }

    @Test
    void isTaskBelongsToOwner_ok() {
        assertDoesNotThrow(() -> subj.isTaskBelongsToOwner(task, owner.getId()));
    }

    @Test
    void isTaskBelongsToOwner_accessDenied() {
        assertThrows(AccessDeniedException.class, () -> subj.isTaskBelongsToOwner(task, 2L));
    }

    @Test
    void convertStringToLocalDateTime_ValidDateString_ReturnsLocalDateTime() {
        LocalDateTime expectedDateTime = LocalDateTime.of(2024, 3, 25, 13, 30, 0);

        LocalDateTime resultDateTime = LocalDateTime.parse("2024-03-25T13:30:00", DateTimeFormatter.ISO_LOCAL_DATE_TIME);

        assertEquals(expectedDateTime, resultDateTime);
    }

    @Test
    void findByIdAndOwnerId_ok() {
        when(taskRepository.findByIdAndOwnerId(task.getId(), task.getOwner().getId())).thenReturn(task);
        when(taskToTaskDtoConverter.convert(task)).thenReturn(taskDto);

        TaskDto expectedTaskDto = subj.findByIdAndOwnerId(task.getId(), task.getOwner().getId());

        assertNotNull(expectedTaskDto);
        assertEquals(taskDto, expectedTaskDto);

        verify(taskRepository, times(1)).findByIdAndOwnerId(id, task.getOwner().getId());
        verify(taskToTaskDtoConverter, times(1)).convert(task);
    }

    @Test
    void findByIdAndOwnerId_notFound() {
        when(taskRepository.findByIdAndOwnerId(1L, 1L)).thenThrow(NotFoundException.class);

        assertThrows(NotFoundException.class, () -> subj.findByIdAndOwnerId(1L, 1L));
    }

    @Test
    void findAllByOwnerId_ok() {
        List<Task> taskList = singletonList(task);
        List<TaskDto> taskDtoList = singletonList(taskDto);

        when(taskRepository.findAllByOwnerId(1L)).thenReturn(taskList);

        List<TaskDto> expectedTaskDtoList = subj.findAllByOwnerId(1L);
        for (int i = 0; i < expectedTaskDtoList.size(); i++) {
            TaskDto expectedTaskDto = expectedTaskDtoList.get(i);
            TaskDto actualTaskDto = taskDtoList.get(i);

            assertEquals(expectedTaskDto, actualTaskDto);
        }
        verify(taskRepository, times(1)).findAllByOwnerId(1L);
    }

    @Test
    void findAllByOwnerId_emptyList() {
        when(taskRepository.findAllByOwnerId(1L)).thenReturn(new ArrayList<>());

        assertTrue(subj.findAllByOwnerId(1L).isEmpty());

        verify(taskRepository, times(1)).findAllByOwnerId(1L);
    }

    @Test
    void findActiveTasksByTelegramChatId_ok() {
        List<Task> taskList = singletonList(task);
        List<TaskDto> taskDtoList = singletonList(taskDto);
        when(taskRepository.findActiveTasksByTelegramChatId(telegramChatId)).thenReturn(taskList);

        List<TaskDto> activeTasksByTelegramChatId = subj.findActiveTasksByTelegramChatId(telegramChatId);
        for (int i = 0; i < activeTasksByTelegramChatId.size(); i++) {
            TaskDto expectedTaskDto = activeTasksByTelegramChatId.get(i);
            TaskDto actualTaskDto = taskDtoList.get(i);

            assertEquals(expectedTaskDto, actualTaskDto);
        }

        verify(taskRepository, times(1)).findActiveTasksByTelegramChatId(telegramChatId);
    }

    @Test
    void findClosedTasksByTelegramChatId_ok() {
        Task task = new Task().setStatus(StatusCode.CLOSED)
                              .setPerformers(singleton(new Client().setTelegramChatId(telegramChatId)));
        List<Task> taskList = singletonList(task);

        TaskDto taskDto = new TaskDto().setStatus(StatusCode.CLOSED.name())
                                       .setPerformers(Collections.singleton(new ClientDto().setTelegramChatId(telegramChatId)));
        List<TaskDto> taskDtoList = singletonList(taskDto);

        when(taskRepository.findClosedTasksByTelegramChatId(telegramChatId)).thenReturn(taskList);


        List<TaskDto> closedTasksByTelegramChatId = subj.findClosedTasksByTelegramChatId(telegramChatId);

        for (int i = 0; i < closedTasksByTelegramChatId.size(); i++) {
            TaskDto expectedTaskDto = closedTasksByTelegramChatId.get(i);
            TaskDto actualTaskDto = taskDtoList.get(i);

            assertEquals(expectedTaskDto, actualTaskDto);
        }

        verify(taskRepository, times(1)).findClosedTasksByTelegramChatId(telegramChatId);
    }

    @Test
    void findMyCreatedAndActiveTasksByTelegramChatId_ok() {
        List<Task> taskList = singletonList(task);
        List<TaskDto> taskDtoList = singletonList(taskDto);

        when(taskRepository.findMyCreatedAndActiveTasksByTelegramChatId(telegramChatId)).thenReturn(taskList);

        List<TaskDto> myCreatedAndActiveTasksByTelegramChatId = subj.findMyCreatedAndActiveTasksByTelegramChatId(telegramChatId);

        for (int i = 0; i < myCreatedAndActiveTasksByTelegramChatId.size(); i++) {
            TaskDto expectedTaskDto = myCreatedAndActiveTasksByTelegramChatId.get(i);
            TaskDto actualTaskDto = taskDtoList.get(i);

            assertEquals(expectedTaskDto, actualTaskDto);
        }

        verify(taskRepository, times(1)).findMyCreatedAndActiveTasksByTelegramChatId(telegramChatId);
    }

    @Test
    void getAllTasksByDeadline_ok() {
        List<Task> taskList = singletonList(task);

        LocalDateTime localDateTime = LocalDateTime.now();
        when(taskRepository.findAllByDeadline(localDateTime)).thenReturn(taskList);

        List<Task> allTasksByDeadline = subj.getAllTasksByDeadline(localDateTime);

        for (int i = 0; i < allTasksByDeadline.size(); i++) {
            Task expectedTask = allTasksByDeadline.get(i);
            Task actualTask = taskList.get(i);

            assertEquals(expectedTask, actualTask);
        }

        verify(taskRepository, times(1)).findAllByDeadline(localDateTime);
    }

    @Test
    void getAllTasksByDayTime_ok() {
        List<Task> taskList = singletonList(task);

        LocalTime localTime = LocalTime.now();
        DayOfWeek dayOfWeek = DayOfWeek.FRIDAY;

        when(taskRepository.findAllByDayTime(dayOfWeek, localTime)).thenReturn(taskList);

        List<Task> allTasksByDayTime = subj.getAllTasksByDayTime(dayOfWeek, localTime);

        for (int i = 0; i < allTasksByDayTime.size(); i++) {
            Task expectedTask = allTasksByDayTime.get(i);
            Task actualTask = taskList.get(i);

            assertEquals(expectedTask, actualTask);
        }

        verify(taskRepository, times(1)).findAllByDayTime(dayOfWeek, localTime);
    }

    @Test
    void getAllTasksByNotificationType_ok() {
        Task taskOne = new Task().setNotificationType(notificationType);
        Task taskTwo = new Task().setNotificationType(notificationType);

        List<Task> tasksWithTypeHour = Arrays.asList(taskOne, taskTwo);

        when(taskRepository.findAllByNotificationType(notificationType)).thenReturn(tasksWithTypeHour);

        List<Task> allExpectedTasksTypeHour = subj.getAllTasksByNotificationType(notificationType);
        for (int i = 0; i < allExpectedTasksTypeHour.size(); i++) {
            Task expectedTask = allExpectedTasksTypeHour.get(i);
            Task actualTask = tasksWithTypeHour.get(i);

            assertEquals(expectedTask.getNotificationType(), actualTask.getNotificationType());
        }

        verify(taskRepository, times(1)).findAllByNotificationType(notificationType);
    }
}