package com.example.taskManager.service;

import com.example.taskManager.entity.Client;
import com.example.taskManager.exception.NotFoundException;
import com.example.taskManager.exception.ServiceException;
import com.example.taskManager.repository.ClientRepository;
import com.example.taskManager.security.ClientRole;
import com.example.taskManager.security.CustomGrantedAuthority;
import com.example.taskManager.security.CustomUserDetails;
import com.example.taskManager.service.dto.ClientDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {
    private final Long id = 1L;
    private final String password = "myPassword";
    private final String surname = "mySurname";
    private final String name = "myName";
    private final String patronymic = "myPatronymic";
    private final String email = "myEmail@mail.ru";
    private final Long telegramChatId = 1234L;
    private final String role = "SUPERVISOR";
    private final String encodedPassword = "encodedPassword";

    @InjectMocks
    ClientService subj;

    @Mock
    ClientRepository clientRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    Converter<Client, ClientDto> clientClientDtoConverter;

    @Test
    void register_ok() {
        Client client = new Client().setSurname(surname)
                                    .setName(name)
                                    .setPatronymic(patronymic)
                                    .setEmail(email)
                                    .setPassword(encodedPassword)
                                    .setTelegramChatId(null)
                                    .setRole(ClientRole.valueOf(role));

        ClientDto expectedClientDto = new ClientDto().setSurname(client.getSurname())
                                                     .setName(client.getName())
                                                     .setPatronymic(client.getPatronymic())
                                                     .setEmail(client.getEmail())
                                                     .setTelegramChatId(null)
                                                     .setRole(client.getRole().name());

        when(clientRepository.save(client)).thenReturn(client);
        when(passwordEncoder.encode(password)).thenReturn(encodedPassword);
        when(clientClientDtoConverter.convert(client)).thenReturn(expectedClientDto);

        assertEquals(expectedClientDto, subj.register(surname, name, patronymic, email, password, role));

        verify(passwordEncoder).encode(password);
        verify(clientRepository).save(client);
        verify(clientClientDtoConverter).convert(client);
    }

    @Test
    void notRegistered_clientExists() {
        when(clientRepository.save(any(Client.class))).thenThrow(new ServiceException("Пользователь с email: " + email + " уже существует!"));

        assertThrows(ServiceException.class, () -> subj.register(surname, name, patronymic, email, password, role));

        verify(clientRepository, times(1)).save(any(Client.class));
        verifyNoInteractions(clientClientDtoConverter);
    }

    @Test
    void findAll_ok() {
        Client client = new Client().setSurname(surname)
                                    .setName(name)
                                    .setPatronymic(patronymic)
                                    .setEmail(email)
                                    .setPassword(encodedPassword)
                                    .setTelegramChatId(telegramChatId)
                                    .setRole(ClientRole.valueOf(role));

        ClientDto expectedClientDto = new ClientDto().setSurname(client.getSurname())
                                                     .setName(client.getName())
                                                     .setPatronymic(client.getPatronymic())
                                                     .setEmail(client.getEmail())
                                                     .setTelegramChatId(client.getTelegramChatId())
                                                     .setRole(client.getRole().name());

        when(clientRepository.findAll()).thenReturn(singletonList(client));
        when(clientClientDtoConverter.convert(client)).thenReturn(expectedClientDto);

        List<ClientDto> clientDtoList = singletonList(expectedClientDto);
        assertEquals(clientDtoList, subj.findAll());

        verify(clientRepository).findAll();
        verify(clientClientDtoConverter).convert(client);
    }

    @Test
    void findAll_notFound() {
        when(clientRepository.findAll()).thenReturn(new ArrayList<>());

        assertEquals(subj.findAll(), new ArrayList<>());
    }

    @Test
    void updateTelegramChatId_ok() {
        Client client = new Client();

        when(clientRepository.findById(id)).thenReturn(Optional.of(client));
        when(clientRepository.save(client)).thenReturn(client);

        subj.updateTelegramChatId(id, telegramChatId);

        assertEquals(telegramChatId, client.getTelegramChatId());
        verify(clientRepository).save(client);
    }

    @Test
    void updateTelegramChatId_telegramChatIdIsNull() {
        Client client = new Client().setId(id);

        subj.updateTelegramChatId(id, null);

        assertNull(client.getTelegramChatId());
    }

    @Test
    void updateTelegramChatId_telegramChatIdIsExists() {
        Client client = new Client().setId(id)
                                    .setTelegramChatId(telegramChatId);

        when(clientRepository.findById(id)).thenReturn(Optional.of(client));

        ServiceException serviceException = assertThrows(ServiceException.class, () -> subj.updateTelegramChatId(id, telegramChatId));

        assertEquals("Telegram Chat ID уже зарегистрирован.", serviceException.getMessage());
        assertEquals(telegramChatId, client.getTelegramChatId());

        verify(clientRepository, never()).save(client);
    }

    @Test
    void getClientByTelegramChatId_ok() {
        Client client = new Client().setSurname(surname)
                                    .setName(name)
                                    .setPatronymic(patronymic)
                                    .setEmail(email)
                                    .setPassword(encodedPassword)
                                    .setTelegramChatId(telegramChatId)
                                    .setRole(ClientRole.valueOf(role));

        ClientDto expectedClientDto = new ClientDto().setSurname(client.getSurname())
                                                     .setName(client.getName())
                                                     .setPatronymic(client.getPatronymic())
                                                     .setEmail(client.getEmail())
                                                     .setTelegramChatId(client.getTelegramChatId())
                                                     .setRole(client.getRole().name());

        when(clientRepository.getClientByTelegramChatId(telegramChatId)).thenReturn(client);
        when(clientClientDtoConverter.convert(client)).thenReturn(expectedClientDto);

        ClientDto clientDto = subj.getClientByTelegramChatId(telegramChatId);

        assertNotNull(clientDto);
        assertEquals(clientDto, expectedClientDto);
    }

    @Test
    void getClientByTelegramChatId_NotFound_ClientIsNull() {
        when(clientRepository.getClientByTelegramChatId(Long.MAX_VALUE)).thenReturn(null);

        ClientDto clientDto = subj.getClientByTelegramChatId(Long.MAX_VALUE);

        assertNull(clientDto);
    }


    @Test
    void isTelegramChatIdRegisteredByClientId_ok() {
        when(clientRepository.existsChatIdByClientId(id)).thenReturn(true);

        assertTrue(subj.isTelegramChatIdRegisteredByClientId(id));
    }

    @Test
    void isTelegramChatIdRegisteredByClientId_chatIdDoesNotRegistered() {
        when(clientRepository.existsChatIdByClientId(id)).thenReturn(false);

        assertFalse(subj.isTelegramChatIdRegisteredByClientId(id));
    }

    @Test
    void currentClientDto_ok() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        CustomUserDetails client = new CustomUserDetails(id, surname, name, patronymic, email, password, telegramChatId, new CustomGrantedAuthority(ClientRole.SUPERVISOR));

        when(authentication.getPrincipal()).thenReturn(client);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        Client clientEntity = new Client().setSurname(surname)
                                          .setName(name)
                                          .setPatronymic(patronymic)
                                          .setEmail(email)
                                          .setPassword(encodedPassword)
                                          .setTelegramChatId(telegramChatId)
                                          .setRole(ClientRole.valueOf(role));

        when(clientRepository.getOne(id)).thenReturn(clientEntity);

        ClientDto expectedClientDto = new ClientDto().setSurname(clientEntity.getSurname())
                                                     .setName(clientEntity.getName())
                                                     .setPatronymic(clientEntity.getPatronymic())
                                                     .setEmail(clientEntity.getEmail())
                                                     .setTelegramChatId(clientEntity.getTelegramChatId())
                                                     .setRole(clientEntity.getRole().name());

        when(clientClientDtoConverter.convert(clientEntity)).thenReturn(expectedClientDto);

        ClientDto result = subj.currentClientDto();

        assertNotNull(result);
        assertEquals(expectedClientDto, result);

        verify(clientRepository).getOne(id);
    }

    @Test
    void currentClientDto_entityNotFound() {
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);

        when(authentication.getPrincipal()).thenReturn(new CustomUserDetails(id, surname, name, patronymic, email, password, telegramChatId, new CustomGrantedAuthority(ClientRole.SUPERVISOR)));
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        when(clientRepository.getOne(any())).thenThrow(EntityNotFoundException.class);

        assertThrows(NotFoundException.class, () -> subj.currentClientDto());
    }
}