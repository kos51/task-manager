package com.example.taskManager.security;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import javax.servlet.ServletException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CustomAuthenticationFailureHandlerTest {

    @Test
    void onAuthenticationFailure_sessionExpired() throws IOException, ServletException {
        CustomAuthenticationFailureHandler failureHandler = new CustomAuthenticationFailureHandler();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        AuthenticationException exception = new SessionAuthenticationException("Session expired");

        failureHandler.onAuthenticationFailure(request, response, exception);

        assertEquals("/login?expired=true", response.getRedirectedUrl());
    }

    @Test
    void onAuthenticationFailure_badCredentials() throws IOException, ServletException {
        CustomAuthenticationFailureHandler failureHandler = new CustomAuthenticationFailureHandler();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        AuthenticationException exception = new BadCredentialsException("Invalid credentials");

        failureHandler.onAuthenticationFailure(request, response, exception);

        assertEquals("/login?error=true", response.getRedirectedUrl());
    }

    @Test
    void onAuthenticationFailure_disabledUser() throws IOException, ServletException {
        CustomAuthenticationFailureHandler failureHandler = new CustomAuthenticationFailureHandler();
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        AuthenticationException exception = new DisabledException("User account is disabled");

        failureHandler.onAuthenticationFailure(request, response, exception);

        assertEquals("/login?error=true", response.getRedirectedUrl());
    }

}