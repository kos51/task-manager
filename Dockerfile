FROM openjdk:8

RUN mkdir -p /root/srv/app
RUN mkdir -p /root/srv/app/env
RUN mkdir -p /root/srv/app/data

COPY . /root/srv/app/
COPY target/task-manager-0.0.1-SNAPSHOT.jar /root/srv/app/application.jar
COPY src/main/resources/application-dev.yml /root/srv/app/env/application-dev.yml

WORKDIR /root/srv/app

ENTRYPOINT ["java", "-jar", "application.jar"]